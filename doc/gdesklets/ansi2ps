#! /usr/bin/env python
"""
-------------------------------------------------------------------------------
ansi2ps, written by S.Fourmanoit, 2005.

Small python >=2.3 script that convert an input on stdin containing
ANSI escaped colors to conforming postscript level 2.

Mostly useful as a first step toward the creation of encapsultated
postscript representation of terminal output.
-------------------------------------------------------------------------------
"""
import re
from sys import stdin, stdout
from itertools import count, izip
from math import ceil

# ansi color to rgb function
#
def ansi2rgb(ansi):
    for component in ansi:
        if component in range(30,39):
            return [ (0,1)[((component-30) & 2**pow)!=0]
                     for pow in range(0,3)]

# rgb triplet to postscript 3 values
#
def rgb2ps(iterable):
    if iterable:
        return reduce(lambda x,y: x + ' ' + y, [str(x) for x in iterable])
    else:
        return '0 0 0'
    
# Read the whole content in
#
content = stdin.read()

# Identify all escaped sequences giving color formatting
#
ansi = [ [ [int(val) for val in group.split(';') ]
           for group in expr.groups() ]
         for expr in re.finditer('\033\[([0-9;]+)m',content) ]
ansi.insert(0,[[0,30]])

# Create postscript header
#
pages=ceil(len(content.split('\n'))/float(63))
print """%!PS-Adobe-3.0 EPSF-3.0
%%Creator: ansi2ps, written by S.Fourmanoit, 2005.
%%LanguageLevel: 2
%%BoundingBox: 0 0 612 792"""
print '%%%%Pages: %d' % pages
print """
%-------------------------------------------------
% Headers
%-------------------------------------------------
/Times-Roman findfont
12 scalefont
setfont

/cr {
   currentpoint
   12 sub
   exch pop 0 exch
   moveto
} def
%-------------------------------------------------
% Content
%-------------------------------------------------
"""
print '%%%%Page: 1 %d\n0 780 moveto' % pages

# Output the content
#
i=0
for color, chunk in zip(ansi, re.split('\033\[[0-9;]+m',content)):
    print '%s setrgbcolor' % rgb2ps(ansi2rgb(color[0]))
    cr=False
    for line in re.split('(\n)',chunk):
        if line!='\n':
            print '(%s) show' % line
        else:
            if i%63==0:
                if i!=0:
                    print 'showpage'
                    print '%%%%Page: %d %d\n0 780 moveto' % (i/63+1,pages)
            else:
                print 'cr'
            i+=1
print 'showpage\n%%EOF'
