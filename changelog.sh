#! /bin/sh
#-------------------------------------------------------------------------------
# Written by S.Fourmanoit <syfou@users.sourceforge.net>, 2005
#
# Helper script that semi-automate the extraction of entries from
# git repository, producing minimaly compliant GNU ChangeLog.
#
# Note: Output is not 'as is' a good ChangeLog; things should be edited
# by hand.
#
#-------------------------------------------------------------------------------
#
ID=`cg tag-ls | sed 's/adesklets-//;s/_/ /g' | \
    sort -n | gawk '{print $1*10000+$2*100+$3, $4}' | \
    sort -r -n | head -n1 | gawk '{print $2}'`

echo -e "$(date +%Y-%m-%d)\t S.Fourmanoit <syfou@users.sourceforge.net>\n"
cg log -f -r $ID: | \
sed -n '/BKrev:/d;/^[[:space:]]*$/d;/(Logical change/d;/^[[:space:]]/p' | \
gawk 'BEGIN {ok=1} /^[ \t]*\* site/ {ok=0; next} /^[ \t]*\*/ {ok=1} ok==1 {print}'
