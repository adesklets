#! /bin/sh
DATE=$(date -u)
FILE=adesklets-`date -u +'%m%d%y_%Hh%M' --date="$DATE"`.diff
cat <<EOF
=== Description ====================================
Original filename - $FILE
Machine           - $(whoami)@$(hostname)
Date              - $DATE
Description       - automatically generated 
                    patch for adesklets

=== Usage ==========================================
Apply with the command:
\`patch -p 1 < $FILE'
from $(basename $*) base directory. 
Do not forget to rerun \`autoreconf'
after patching.

=== Additional comment =============================
See the \`devpatch' target from Makefile.am
for details.
====================================================
EOF
