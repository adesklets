"""
test_events.py - S.Fourmanoit <syfou@users.sourceforge.net>, 2005

Small, non-exhaustive adesklets test script that test dynamic remapping
of events through the Events_handler::get_events() and
Events_handler::set_events(), introduced in adesklets 0.3.0.

All it does is to create a 100x100 pseudo-transparent managed window,
then catch MotionNotify events until clicked onto.

To try it:
	- Install adesklets (>=0.3.0) with python support enabled (default)
	- Run python test_events.py from this directory
"""
import adesklets

class My_Events(adesklets.Events_handler):
    def __init__(self):
        adesklets.Events_handler.__init__(self)
        
    def __del__(self):
        adesklets.Events_handler.__del__(self)
        
    def ready(self):
        adesklets.window_resize(100,100)
        adesklets.window_reset(adesklets.WINDOW_MANAGED)
        adesklets.window_set_transparency(True)
        adesklets.window_show()
        
    def quit(self):
        print 'Quitting...'
        
    def alarm(self):
        print 'Alarm. Next in 10 seconds.'
        return 10
    
    def motion_notify(self, delayed, x, y):
        print 'Motion notify:', x, y, delayed

    def button_press(self,delayed,x,y,button):
        print '================= ',
        events=self.get_events()
        if events['MotionNotify']:
            events['MotionNotify']=None
            print 'stop',
        else:
            events['MotionNotify']=My_Events.motion_notify
            print 'start',
        print ' ================='
        self.set_events(events)
        
My_Events().pause()
