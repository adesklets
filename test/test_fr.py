"""
test_fr.py - S.Fourmanoit <syfou@users.sourceforge.net>,
	     Guillaume Boitel <g.boitel@wanadoo.fr>

Petit script de test non-exhaustif pour adesklets :

      - Redimensionne la fen�tre adesklets � 100x100 pixels
      - La met sous le contr�le du gestionnaire de fen�tre
      - La rend pseudo-transparente
      - L'affiche � l'�cran
      - Attends ensuite que l'utilisateur la quitte,
        g�n�re une alarme toutes les 10 secondes
        et attrape les �v�nements motion_notify d�s qu'ils apparaissent.

Pour l'essayer :
      - Installer adesklets avec le support python activ� (par d�faut)
      - Lancer python test.py � partir de ce r�pertoire.
"""
import adesklets

class My_Events(adesklets.Events_handler):
    def __init__(self):
        adesklets.Events_handler.__init__(self)
        
    def __del__(self):
        adesklets.Events_handler.__del__(self)
        
    def ready(self):
        adesklets.window_resize(100,100)
        adesklets.window_reset(adesklets.WINDOW_MANAGED)
        adesklets.window_set_transparency(True)
        adesklets.window_show()

    def quit(self):
        print 'Quitting...'
        
    def alarm(self):
        print 'Alarm. Next in 10 seconds.'
        return 10
    
    def motion_notify(self, delayed, x, y):
        print 'Motion notify:', x, y, delayed

My_Events().pause()
