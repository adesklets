"""
bumpmap.py - S.Fourmanoit <syfou@users.sourceforge.net>, 2005

Short, but more involved adesklets test script. It demonstrates
Imlib2 dynamic filtering capabilities and how you can modify
a background image dynamically.

Note: I strongly advise against using an effect that CPU intensive
in real life desklets - remember adesklets was made to be 'light'.

To try it:
	- Install adesklets with python support enabled (default)
	- Run python bumpmap.py from this directory.
"""
import adesklets

class My_Events(adesklets.Events_handler):
    x = 100	# Initial spot position
    y = 100
    
    def __init__(self):
        adesklets.Events_handler.__init__(self)
        
    def __del__(self):
        adesklets.Events_handler.__del__(self)
        
    def ready(self):
        adesklets.window_resize(200,200)
        adesklets.window_set_background_image(adesklets.clone_image())
        adesklets.window_set_transparency(True)
        adesklets.window_reset(adesklets.WINDOW_MANAGED)

        # Now, let us set a 'Test!' text on foreground image:
        # we will not modify it any more
        #
        adesklets.context_set_image(0)
        adesklets.context_set_font(adesklets.load_font('Vera/60'))
        adesklets.context_set_color(255,0,0,200)
        adesklets.context_set_direction(adesklets.TEXT_TO_ANGLE)
        adesklets.context_set_angle(45)
        adesklets.text_draw(20,0,'Test!')
        adesklets.free_font(0)
        
        adesklets.window_show()
        
    def background_grab(self,delayed):
        self._display()
        
    def motion_notify(self, delayed, x, y):
        if not delayed:
            self.x = x
            self.y = y
            self._display()

    def _display(self):
        """
        The  drawing method. Please remember this nomenclature:
        
          image 0: foreground image, left untouched here
          image 1: original background, automatically kept up to date
                   by adesklets - hence we keep ourselves from modify it.
          image 2: Our displayed background - set My_Events::ready() for
                   initialisation details
          image 3: transient image on which we operate the
                   current transformation, before updating image 2
                   and destroying it.
        """
        adesklets.context_set_image(1)
        adesklets.context_set_image(adesklets.clone_image())
        adesklets.apply_filter("bump_map_point(x=%d,y=%d);" % (self.x, self.y))
        adesklets.context_set_image(2)
        adesklets.blend_image_onto_image(3,0,0,0,200,200,0,0,200,200)
        adesklets.free_image(3)
        
My_Events().pause()
