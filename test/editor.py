"""
editor.py - S.Fourmanoit <syfou@users.sourceforge.net>, 2005

adesklets test script for editor launch, similar to what the Configure Menu
in many desklet returns, but with pause. You have to exit the editor for
the script to continue. Start with:

python editor.py
"""
from os import environ, getenv, system, unlink
from os.path import join, isfile

# Test function
#
def launch(msg):
    print msg
    print >> file('/tmp/editor.txt','w'), msg
    if len(display):
        if len(xterm) > 0:
            if len(editor) > 0:
                cmdline = '%s -e %s /tmp/editor.txt' % (xterm[0], editor)
                print "Command line is '%s'." % cmdline
                print 'Command line returned %d (0 is OK)' % system(cmdline) 
            else:
                print 'editor not found.'
        else:
            print 'xterm not found.'
    else:
        print 'display not found.'

# Get the pertinent information
#
display = getenv('DISPLAY','')
xterm   = [join(p,'xterm')
           for p in getenv('PATH','').split(':') if isfile(join(p,'xterm'))]
editor  = getenv('EDITOR','')

# Display this information
#
print """=== System information ===

Display     : '%s'
xterm paths : %s
Editor      : '%s'
""" % (editor, display, xterm)

# Now, let's test the launch.
#
launch('Launching editor in xterm without adesklets.')
environ['ADESKLETS_ID'] = '0'; import adesklets
launch('Launching editor in xterm with adesklets.')
print 'Now exiting.'
