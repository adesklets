"""
fading.py - S.Fourmanoit <syfou@users.sourceforge.net>, 2005

adesklets test script for indirect mode, introduced in adesklets 0.2.0:

- It creates a transpaent window of the same size than supplied image
  ('image/frog.png'; you do not like it? Maybe, but it's mine)
- It then draws the image on in
- It checks for the cursor entering and leaving its window visible parts,
  firing a half second fade-out effect (ten frames) on leaving,
  interrupting it as needed by new cursor change of position

To try it:
	- Install adesklets with both python support enabled (default)
          and GNU history support (default)
	- Run python fading.py from this directory
"""
import adesklets

class My_Events(adesklets.Events_handler):
    witdh  = 1
    height = 1
    fade   = ()
    image  = 'images/frog.png'
    
    def __init__(self):
        adesklets.Events_handler.__init__(self)
        
    def __del__(self):
        adesklets.Events_handler.__del__(self)
        
    def ready(self):
        # Load the image, set the window accordingly
        #
        adesklets.context_set_image(adesklets.load_image(self.image))
        self.width=adesklets.image_get_width();
        self.height=adesklets.image_get_height();
        adesklets.window_resize(self.width,self.height)

        # Let's create our ten step fading tool: a plain color modifier
        #
        adesklets.context_set_color_modifier(adesklets.create_color_modifier())
        adesklets.set_color_modifier_tables(
            range(0,256)*3 + [max(0,val-26) for val in range(0,256)])

        # Now, let's toggle the interpreter mode, 
        # and record the complete fading sequence
        #
        adesklets.start_recording()
        for i in range(1,11):
            adesklets.context_set_image(3)
            adesklets.apply_color_modifier()
            adesklets.context_set_image(0)
            adesklets.blend_image_onto_image(3,1,
                                             0,0,self.width,self.height,
                                             0,0,self.width,self.height)
            adesklets.time_gate(i*.05)
        self.fade = adesklets.stop_recording()

        # Let's finish seting up the window
        #
        adesklets.context_set_blend(False)
        self._display()
        adesklets.play_set_abort_on_events(True)  # <== Note this !
        adesklets.window_set_transparency(True);
        adesklets.window_show()

    def leave_notify(self,delayed,x,y):
        # Fade-out event handler...
        #
        if not delayed:
            print 'Fading out...',
            adesklets.context_set_image(2)
            adesklets.clone_image()
            if adesklets.play(*self.fade) is None:
                print 'ended.'
            else:
                print 'interrupted.'
            adesklets.free_image(3)

    def menu_fire(self,delayed,id,str):
        # This will get generated first whenever
        # the user moves the window, so the LeaveNotify
        # afterwards will get delayed, thus go unseen.
        pass
        
    def enter_notify(self,delayed,x,y):
        # ...that can be aborted (not interrupted!) by this one.
        #
        self._display()
        
    def _display(self):
        # Reset the window to the original image
        #
        adesklets.context_set_image(0)
        adesklets.blend_image_onto_image(2,1,
                                         0,0,self.width,self.height,
                                         0,0,self.width,self.height)
My_Events().pause()
