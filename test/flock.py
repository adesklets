"""
flock.py -- Simple exclusive lock fcntl test program in Python.

Usage: python /path/to/lock_test.py

The script will try to open(create if needed), then lock a 'lock_test.txt'
file in the current working directory, write its PID in it, then wait for
the user to press enter, then release the lock, close the file and exit.

Tested on both stock Python 2.3.5 and Python 2.4.2
"""
from sys import stdout, exc_info
from traceback import print_exception
from os import getcwd, getpid
from os.path import join
from fcntl import lockf, LOCK_UN, LOCK_EX

try:
    filename = join(getcwd(),'lock_test.txt')
    print 'Opening or creating "%s"...' % filename,
    f = file(filename,'a')
    print 'ok'
    print 'Acquiring lock...',
    stdout.flush()
    lockf(f,LOCK_EX)
    print 'ok'
    print 'Writing pid (%d)...' % getpid(),
    print >> f, "PID:",  getpid()
    print 'ok'
    raw_input('[Press enter to exit]')
    print 'Releasing lock...',
    lockf(f,LOCK_UN)
    print 'ok'
    print 'Closing file...',
    f.close()
    print 'ok'
except:
    print 'failed'
    print '='*80
    print_exception(*exc_info())
    print '='*80
