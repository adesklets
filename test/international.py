'''
international.py - S.Fourmanoit <syfou@users.sourceforge.net>, 2008

Demo international character support in adesklets. Prints a well known
spanish interjection ("ay!") with the leading inverted exclamation
mark, not available in 7-bit ASCII.

To try it:
	- Install adesklets compiled with iconv support (default) and 
          python (default).
	- Run python international.py from this directory

Random Remarks
--------------
- Below, we relay a unicode string to adesklets as UTF-8, encoding it
  on the fly. This is straightforward, but you could as well use any
  kind of encoding with normal strings, as long as what you feed
  to adesklets match the encoding you set via the last call to
  adesklets.set_charset.

- Needless to say, the font you use need to have the glyphs for the
  characters you want to display: if you try to feed cyrillic to a
  roman font, it will not work.
'''

import adesklets

class My_Events(adesklets.Events_handler):
    text = u'\xa1Ay!'   # In this example, we escape the test unicode
                        # string, but it's just a matter of keeping
                        # the sourcecode in ASCII for
                        # portability. Python had supported other source
                        # code encodings for a long time (see PEP 0263).

    def __init__(self):
        adesklets.Events_handler.__init__(self)
        
    def __del__(self):
        adesklets.Events_handler.__del__(self)
        
    def ready(self):
        adesklets.set_charset('utf-8')

        adesklets.window_resize(100,100)
        adesklets.window_reset(adesklets.WINDOW_MANAGED)

        adesklets.context_set_color(0, 0, 0, 125)
        adesklets.image_fill_rectangle(0, 0, 100, 100)

        font=adesklets.load_font('Vera/30')
        adesklets.context_set_font(font)
        adesklets.context_set_color(255, 255, 255, 255)
        adesklets.context_set_direction(adesklets.TEXT_TO_ANGLE)
        adesklets.context_set_angle(45)
        adesklets.text_draw(7, 2, self.text.encode('utf-8'))
        adesklets.free_font(font)
        adesklets.window_show()
        
    def quit(self):
        print 'Quitting...'

My_Events().pause()
