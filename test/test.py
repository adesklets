"""
test.py - S.Fourmanoit <syfou@users.sourceforge.net>, 2004

Small, non-exhaustive adesklets test script:
	- Resize adesklets window to 100x100 pixels
	- Put it under wm's control
        - Set it to be pseudo-transparent
        - Map it on screen
        - Then wait until the user exits,
          generating an alam event every 10 seconds,
          and catching motion_notify events as they occur

To try it:
	- Install adesklets with python support enabled (default)
	- Run python test.py from this directory
"""
import adesklets

class My_Events(adesklets.Events_handler):
    def __init__(self):
        adesklets.Events_handler.__init__(self)
        
    def __del__(self):
        adesklets.Events_handler.__del__(self)
        
    def ready(self):
        adesklets.window_resize(100,100)
        adesklets.window_reset(adesklets.WINDOW_MANAGED)
        adesklets.window_set_transparency(True)
        adesklets.window_show()
        
    def quit(self):
        print 'Quitting...'
        
    def alarm(self):
        print 'Alarm. Next in 10 seconds.'
        return 10
    
    def motion_notify(self, delayed, x, y):
        print 'Motion notify:', x, y, delayed
        
My_Events().pause()
