# test.pl - Lucas Brutschy <lbrutschy@users.sourceforge.net>, 2006
#
# Small, non-exhaustive adesklets test script:
#        - Resize adesklets window to 100x100 pixels
#        - Set it to be pseudo-transparent
#        - Draw a translucent light grey rectangle on it
#        - Map it on screen
#        - Mark with a red dot the pointer position every time 
#          a user click
#
# To try it:
#        - Install adesklets with perl support enabled (default)
#        - Run perl test.py from this directory

use strict;
use adesklets;

adesklets::open_streams();

# these are just normal adesklet commands
adesklets::window_resize(100,100);
adesklets::window_reset(adesklets::WINDOW_UNMANAGED);
adesklets::window_set_transparency(1);
adesklets::context_set_color(255,255,255,64);
adesklets::image_fill_rectangle(5,5,90,90);
adesklets::window_show();

adesklets::event_loop(ButtonPress=>\&onbutton); # supply a hash of callbacks
adesklets::close_streams();

sub onbutton
{
    my($x,$y) = @_;
    adesklets::context_set_color(255,0,0,255);
    adesklets::image_fill_rectangle($x,$y,3,3);    
}
