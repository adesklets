"""
adesklets' utilities module
"""
from commands import version
from configfile import ConfigFile
import re

#-------------------------------------------------------------------------------
def version_check(ver):
    """
    Returns `True' if required version is lower or egal to
    the current package version, `False' otherwise. For instance:

    version_check('0.0.0') will always returns `True'
    """
    def compute_version(ver):
        return reduce(lambda x,y: int(x)*100+int(y),
                      ver.split()[-1].split('.'))
    return compute_version(ver)<=compute_version(version())

#-------------------------------------------------------------------------------
