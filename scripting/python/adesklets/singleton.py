# -*- coding: latin1 -*-
"""
Module implementing the singleton pattern.

This is a slight rewrite of a recipe from ASPN's python cookbook,
'The Singleton Pattern implemented with Python' by J�rgen Hermann:

http://aspn.activestate.com/ASPN/Cookbook/Python/
"""

from error_handler import *

class _Singleton:
    """
    A python singleton. This can be used in subclass wanting
    a unique instance to be created:

    class SubClass(_Singleton)
	...
        def __init__(self):
            _Singleton.__init__(self,'SubClass_Key')
        ...
    """
    __instances = dict()

    def __init__(self, key):
        """
        Create the singleton instance
        """
        if not _Singleton.__instances.has_key(key):
            _Singleton.__instances.setdefault(key)
        else:
            raise ADESKLETSError(2)

        self.key=key
        self.__dict__['_Singleton__instances'] = _Singleton.__instances

    def __del__(self):
        """
        Delete the singleton instance
        """
        self._Singleton__instances.pop(self.key)

class _States:
    """
    Inspired by the 'Borg pattern' idea from ASPN's python cookbook,
    by Alex Martelli:
    
    http://aspn.activestate.com/ASPN/Cookbook/Python

    Creates a single dictionnary shared by all subclasses of _States.
    """
    __shared_state = {}

    def __init__(self):
        self.__dict__['_States__shared_state']= _States.__shared_state
    def __del__(self):
        pass


    
