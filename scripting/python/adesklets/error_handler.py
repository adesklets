"""
adesklets error handler
"""
import types

ADESKLETSErrors = {
   0:  'generic error',
   1:  'adesklets process exited',
   2:  'only a single class of this type should exists',
   3:  'adesklets interpreter initialization error',
   4:  'adesklets command error',
   5:  'adesklets python initialization aborted'
}

class ADESKLETSError(Exception):
   """
   minimalistic adesklets Exception class
   """
   def __init__(self,value=0,message=''):
      if not (type(message) is types.StringType):
         message=''
      self.args=[value,message]
   def __int__(self):
      if ADESKLETSErrors.has_key(self.args[0]):
         return self.args[0]
      else:
         return 0
   def __str__(self):
      return ADESKLETSErrors[int(self)] + ' - ' + self.args[1]
