"""
Pure python package exposing full adesklets API

This package makes it easy to write python code to interact with
the adesklets interpreter.

For pertinent help, have a look at:

- adesklets.Events_handler
- adesklets.commands
- adesklets.utils
- adesklets.configfile

You may safely invoke:

from adesklets.events_handler import Events_handler
from adesklets.commands import *

if you wish, in place of the staightforward 'import adesklets'
for quicker access to all adesklets features without polluting
your current namespace too much.
"""

__all__ = [ "error_handler",
            "signal_handler", "children_handler",
            "commands_handler", "events_handler",
            "singleton", "communicator", "initializer",
            "strings", "commands", "utils", "configfile" ]

from adesklets.initializer import _Initializer
from adesklets.events_handler import Events_handler
from adesklets.commands import *
from adesklets.utils import *
from sys import exit

if __name__ == "adesklets":
  try:
    _init=_Initializer()
  except ADESKLETSError, e:
    if e.args[0]==5:
      exit()
    else:
      raise e

