"""
Children signal handler module
"""

from signal_handler import Signal_handler
from singleton import _Singleton

class _Children_handler(Signal_handler, _Singleton):
    """
    Class to cleanly kill defunc children
    """
    import os
    waitpid=os.waitpid
    WNOHANG=os.WNOHANG
    
    def __init__(self):
        Signal_handler.__init__(self,['CHLD'],self.kill_defunct)
        _Singleton.__init__(self,'_Children_handler')
        
    def kill_defunct(self,signum,frame):
        """
        kill defunct child
        """
        while 1:
            try:
                pid=self.waitpid(-1,self.WNOHANG)
            except OSError, (errno, strerror):
                if errno==10:                   # No child error
                    break
                else:
                    raise
            if pid[0]==0:                       # No child left?
                break
            
    def __del__(self):
        Signal_handler.__del__(self)
        _Singleton.__del__(self)
