"""
adesklets file name canocalisation module
"""

class Which:
    """
    File name canocalisation class. It works exacly as its unix
    counterpart, except it does not enforce the +x permission
    flag on the file, but only its existence.
    """
    import os
    import os.path
    
    def __init__(self,basename):
        self._basename = basename

    def run(self):
        try:
            return \
                   [ self.os.path.abspath(filename) for filename in
                     self._imap(lambda x,y:self.os.path.join(x,y),
                                [{0:'/nothing',1:self.os.getcwd()}
                                 [len(self._basename.split('/'))>1]] +
                                self.os.getenv('PATH','').split(':'),
                                self._repeat())
                     if self.os.path.isfile(filename) ][0]
        except IndexError:
            return None

    def _repeat(self):
        while True:
            yield self._basename

    def _imap(self,func,iterable1,iterable2):
        iter1=iter(iterable1)
        iter2=iter(iterable2)
        while 1:
            try:
               yield func(iter1.next(),iter2.next())
            except StopIteration:
                break
