#! /bin/ash 
#
# enums.sh
#
#-------------------------------------------------------------------------------
# Copyright (C) 2004, 2005, 2006 Sylvain Fourmanoit <syfou@users.sourceforge.net>
#  
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#  
# The above copyright notice and this permission notice shall be included in
# all copies of the Software and its documentation and acknowledgment shall be
# given in the documentation and software packages that this Software was
# used.
#  
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
#
#-------------------------------------------------------------------------------
# Script used as a base to generate components of enums, 
# as cited in prototypes.sh
#
# Output is a tab-separated two columns of the form:
# 
# item_name	value 
#
# Note: Although this _should_ be portable across UNIXes, 
# it has not been tested (successfully, of course) on anything but:
#
#       - GNU Bourne-Again Shell (GNU bash) 3.00.0 and
#         NetBSD Bourne Shell (ash) 1.6.0
#
# Please also note it will not likely work without GNU sed
# (tested on GNU Streaming EDitor - GNU sed - 4.0.9 ), since
# it makes use of many GNU extensions (characters classes, \U, etc).
#
# Finally, you will need bc (any version: very basic use) to run this
# since we do not use shell arithmetic.
#
#-------------------------------------------------------------------------------
sed -n '/\/\* begin enums \*\//,/\/\* end enums \*\//p' ../src/command.c | \
(
while read LINE; do
    NEW_PREFIX=`echo "$LINE" | \
	sed -n '/\/\* prefix / s/^.*\/\* prefix \([[:alnum:]]\+\) \*\/.*$/\1/p'`
    test "x$NEW_PREFIX" != "x" && { PREFIX=$NEW_PREFIX ; I=0; }
    ITEM=`echo "$LINE" | sed -n 's/^[[:space:]]*"\([[:alnum:]_]\+\)".*/\U\1/p'`
    test "x$ITEM" != "x" && {
	echo $ITEM | sed '/^'"$PREFIX"'/!s/^/'"$PREFIX"'_/;s/$/\t'"$I"'/'
	I=`echo "$I + 1" | bc`
    }
done
)

exit 0

