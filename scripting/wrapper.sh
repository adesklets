#! /bin/sh 
#
# wrapper.sh
#
#-------------------------------------------------------------------------------
# Copyright (C) 2004, 2005, 2006 Sylvain Fourmanoit <syfou@users.sourceforge.net>
#  
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#  
# The above copyright notice and this permission notice shall be included in
# all copies of the Software and its documentation and acknowledgment shall be
# given in the documentation and software packages that this Software was
# used.
#  
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
#
#-------------------------------------------------------------------------------
# Wrapper script between Makefile and other scripts. Usage:
#
# ./wrapper.sh my_script out_file
# 
# Redirect my_script stdout to out_file. If script fails, remove out_file.
# If script is not in current working directory, script name need to be 
# absolute. Always echo script exit status, and duplicate script stdout on
# ./wrapper.sh' stderr
#
#-------------------------------------------------------------------------------
if test "x$1" != "x" ; then
    if test "x$2" != "x" ; then
	SCRIPT=`echo "$1" | sed '/^\//!s/^/\.\//'`
	if test -x "$SCRIPT" ; then
	    if ! eval "$SCRIPT" | (
		    # This is a 'tee' emulator: demux the command output
		    # both on stdout and stderr
		    while read LINE; do
			echo "$LINE"
			echo "$LINE" 1>&2
		    done
		) >$2; then
		rm -f $2 2> /dev/null
		exit 1
	    fi
	else
	    echo "could not execute script '$SCRIPT'" 1>&2
	    exit 1
	fi
    else
	echo "output file name not given" 1>&2
	exit 1
    fi
else
    echo "script name not given" 1>&2
    exit 1
fi
