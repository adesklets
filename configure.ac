nl Initialize autoconf and automake
AC_INIT
AC_CONFIG_SRCDIR(src/main.c)
AC_PREREQ(2.52)
AM_INIT_AUTOMAKE(adesklets,0.6.2)
AM_MAINTAINER_MODE

dnl Language selection
AC_LANG(C)

dnl Locate required external software
AC_PROG_CPP
if test "x$CPP" != 'x'; then
:
AC_SUBST(CPP)
fi
AC_PROG_CC
AM_PROG_LEX
if test "x$LEX" != 'xflex'; then
AC_MSG_WARN([
-----------------------------------------------------
`flex' was not found on your system. If you encounter
problems recompiling a `l' file, please try `flex' 
first. You can get it from any GNU archive site. 
You will not need `flex' as long as you do not 
modify `l' files.
----------------------------------------------------])
fi
AC_PROG_YACC
if test "x$YACC" != "xbison -y"; then
AC_MSG_WARN([
-----------------------------------------------------
Nor `byacc' or `yacc' parsers had been used
for developing adesklets. If you encounter problems
recompiling a `y' file, please try `bison' instead. 
You can get it from any GNU archive site. You will 
not need `bison' as long as you do not modify 
`y' files.
-----------------------------------------------------])
fi
AC_PROG_INSTALL

dnl Check for python support
withval=
AC_ARG_WITH(python-support,
[  --without-python-support   
                          do not install adesklets python package
                          Do not set this option unless you have
			  good reasons])
: ${withval:=yes}
if test x$withval = "xyes"; then
withval=
AC_ARG_WITH(python-force-detection,
[  --with-python-force-detection
                          bypass all the python version check mechanism. 
			  When using this, make sure you have a version
			  greater or egal to python 2.3 installed 
			  as 'python' in your path [default=no]])
: ${withval:=no}
if test x$withval = "xno"; then
AM_PATH_PYTHON([2.3])
else
AM_PATH_PYTHON()
fi
AC_PYTHON_DEVEL
withval=
AC_ARG_WITH(python-install-submission-scripts,
[  --with-python-install-submission-scripts
			  install in the path the submission-related scripts
			  from submit/ in the $PREFIX/bin program path.
			  Useful for desklets author [default=no]])
: ${withval:=no}
if test x$withval = "xyes"; then
AM_CONDITIONAL(PYTHON_INSTALL_SUBMISSION_SCRIPTS, true)
else
AM_CONDITIONAL(PYTHON_INSTALL_SUBMISSION_SCRIPTS, false)
fi
AC_CHECK_FUNCS(sigprocmask,
[HAVE_SIGPROCMASK="#define HAVE_SIGPROCMASK 1", SIGPROC=yes],
[HAVE_SIGPROCMASK="#undef HAVE_SIGPROCMASK"])
else
HAVE_SIGPROCMASK="#undef HAVE_SIGPROCMASK"
fi
AC_SUBST(HAVE_SIGPROCMASK)
AM_CONDITIONAL(PYTHON_SUPPORT, test "x$PYTHON" != "x")
AM_CONDITIONAL(PYTHON_INSTALL_SUBMISSION_SCRIPTS, true)

dnl Check for Perl support
withval=
AC_ARG_WITH(perl-support,
[  --without-perl-support  do not install adesklets Perl package
                          Do not set this option unless you have
			  good reasons])
: ${withval:=yes}
perl_ok=false
if test x$withval = "xyes"; then
AC_PATH_PROG(PERL, perl)
AC_PROG_PERL_VERSION(5.8.2,
	AC_PROG_PERL_MODULES(IPC::Open3, perl_ok=true),
	AC_MSG_WARN(['Perl >= 5.8.2 could not be found']))
fi
AM_CONDITIONAL(PERL_SUPPORT,$perl_ok)

dnl help2man presence
AC_PATH_PROG(HELP2MAN, help2man)
AM_CONDITIONAL(HELP2MAN_SUPPORT, test "x$HELP2MAN" != "x")

dnl htmltidy presence
AC_PATH_PROG(TIDY,tidy)

dnl doxygen presence
AC_PATH_PROG(DOXYGEN,doxygen)
AM_CONDITIONAL(DOXYGEN_SUPPORT, test "x$DOXYGEN" != "x")

dnl makeinfo support:
dnl Also test the presence of the full documentation package
AM_CONDITIONAL(MAKEINFO_SUPPORT, 
test "x$MAKEINFO:" != "x" && test -f doc/Makefile.am)

dnl Try to enforce ISO C (iso9899 1999)
dnl This will more likely work
dnl only with gcc. In fact,
dnl expect for variadic macros, this program
dnl should be ansi C (ISO C90).
TMP_CFLAGS="$CFLAGS"
CFLAGS="$CFLAGS -std=c99 -pedantic -Wall"
AC_MSG_CHECKING([for ISO C99 warnings generation])
AC_TRY_COMPILE(
,
int 
main(void) 
{
   return 0L;
},
AC_MSG_RESULT([yes]),
AC_MSG_RESULT([no])
CFLAGS="$TMP_CFLAGS"
)

dnl Define source to be BSD compliant (ISO C, POSIX, and 4.3BSD)
AC_DEFINE(_BSD_SOURCE,1,[Source is BSD compliant (ISO C, POSIX and 4.3BSD)])
if test "x`uname`" = xNetBSD; then
dnl NetBSD (at least version 1.6.1) is following the standard less closely than 
dnl the other... Hence, we declare the source specifically as _POSIX_SOURCE 
dnl instead of the newer _POSIX_C_SOURCE 199309
AC_DEFINE(_POSIX_SOURCE,1,[Source is POSIX 1 compliant (IEEE Std 1003.1)])
else
dnl For some unspecified reason, it as been reported than FreeBSD [5|6|7].x 
dnl doesn't like those POSIX keywords either
UNAME_REDUX=`uname -r -s | sed 's/\..*//' 2> /dev/null`
if test "$UNAME_REDUX" != 'FreeBSD 5' && \
   test "$UNAME_REDUX" != 'FreeBSD 6' && \
   test "$UNAME_REDUX" != 'FreeBSD 7'
then
AC_DEFINE(_POSIX_C_SOURCE,199309,[Source is POSIX 4 compliant (IEEE Std 1003.1b)])
fi
fi

dnl Check for system header files
AC_HEADER_STDC
AC_CHECK_HEADERS(stdio.h stdarg.h errno.h fcntl.h signal.h time.h math.h sys/time.h sys/types.h sys/stat.h grp.h pwd.h libgen.h dirent.h iconv.h)

dnl Enable debugging
dnl This will filter out optimisation switch (-O[s0-9]) 
dnl for gcc compiler
AC_ARG_ENABLE(
  debug, [  --enable-debug	  enable debugging symbols and supplementary 
                          messages generation in final binary])

: ${enableval="no"}
AC_MSG_CHECKING([for debugging symbols generation])
if test x$enableval = "xyes"; then
   AC_MSG_RESULT([yes])
   AC_DEFINE(DEBUG, 1, [Debug mode is on if defined.])
   CFLAGS="$CFLAGS -g"
   DEBUG=yes
else
   AC_MSG_RESULT([no])
   CFLAGS=`echo "$CFLAGS" | sed 's/^-g$//; s/^-g@<:@@<:@:space:@:>@@:>@\+// ; s/@<:@@<:@:space:@:>@@:>@\+-g$//; s/@<:@@<:@:space:@:>@@:>@\+-g@<:@@<:@:space:@:>@@:>@\+/ /'`
fi

dnl SIGKILL timeout setting
enableval=
AC_MSG_CHECKING([for sigkill timeout])
AC_ARG_ENABLE(sigkill-timeout,
[  --enable-sigkill-timeout=SEC
                          timeout (in seconds) before sending `kill'
                          signal to parent process in case of restart 
                          if `term' has no effect [default=5]])
: ${enableval:=5}
test $enableval -gt 0 2> /dev/null || {
AC_MSG_RESULT([no])
AC_MSG_ERROR([Given sigkill timeout was not understood])
}
AC_MSG_RESULT([yes])
AC_DEFINE_UNQUOTED(SIGKILL_TIMEOUT,$enableval,
[Delay between SIGTERM ans SIGKILL to parent process in case of restart.])

dnl X polling frequency setting
enableval=
period=
AC_MSG_CHECKING([for polling frequency])
AC_ARG_ENABLE(x-polling-frequency,
[  --enable-x-polling-frequency=FREQ
                          default frequency (in hertz) for polling the X server
                          for events. Lower values leads to less responsive,
                          but less CPU hungry desklets [default=50]])
: ${enableval:=50}
X_POLLING_FREQ=$enableval
test $enableval -gt 0 2> /dev/null || {
AC_MSG_RESULT([no])
AC_MSG_ERROR([Positive, integer value is required as x-polling-frequency])
}

AC_CHECK_PROG(EXISTS,bc,yes,no)
if test $enableval -ne 50; then
if test x$EXISTS = "xyes"; then
   period=`echo "1000000 / $enableval" | bc`
else
   AC_MSG_WARN([Could not find the `bc' calculator to perform arithmetic 
computation of X server polling period value. Default 50 Hz is assumed.])
fi
fi
: ${period:=20000}
AC_DEFINE_UNQUOTED(X_POLLING_PERIOD,$period,
[Default polling period (in 10e-6 seconds) of stdin between two cycles of events loop.])
X_POLLING_PERIOD=$period
AC_SUBST(X_POLLING_FREQ)

dnl Lock files directory setting
enableval=
AC_MSG_CHECKING([for lock files directory setting])
AC_ARG_ENABLE(lockfiles-dir,
[  --enable-lockfiles-dir=DIR
                          directory to put lock files in [default=/tmp]])
: ${enableval:=/tmp}
test -d $enableval || {
AC_MSG_RESULT([no])
AC_MSG_ERROR([Given lock files directory does not exist on your system])
}
AC_MSG_RESULT([yes])
AC_DEFINE_UNQUOTED(LOCKFILES_DIR,"$enableval",
[Directory to put lock files in.])
LOCKFILES_DIR=$enableval
AC_SUBST(LOCKFILES_DIR)

dnl Control modifier on context menu
enableval=
AC_MSG_CHECKING([for control modifier on context menu])
AC_ARG_ENABLE(control-on-context-menu,
[  --enable-control-on-context-menu
                          force CTRL to be pressed to fire context menu
                          [default=no]])
: ${enableval:=no}
if test x$enableval != "xno" ; then
AC_MSG_RESULT([yes])
AC_DEFINE(CONTROL_ON_CONTEXT_MENU, 1,
[Force CTRL to be pressed to fire context menu])
else
AC_MSG_RESULT([no])
fi

dnl Legacy Window Manager detection code
enableval=
AC_MSG_CHECKING([for legacy fake root window detection code])
AC_ARG_ENABLE(legacy-fake-root-window-detection,
[  --enable-legacy-fake-root-window-detection
                          Use the old code to detect fake root windows
                          [default=no]])
: ${enableval:=no}
if test x$enableval != "xno" ; then
AC_MSG_RESULT([yes])
AC_DEFINE(WM_DETECTION_OLD_STYLE, 1,
[Use former fake root window detection code])
else
AC_MSG_RESULT([no])
fi

dnl Legacy pseudo transparency
enableval=
AC_MSG_CHECKING([for legacy pseudo transparency])
AC_ARG_ENABLE(legacy-pseudo-transparency,
[  --enable-legacy-pseudo-transparency
                          Use the old code implementing pseudo-transparency
                          grabbing [default=no]])
: ${enableval:=no}
if test x$enableval != "xno" ; then
AC_MSG_RESULT([yes])
AC_DEFINE(FAKE_TRANSPARENCY_OLD_STYLE, 1,
[Use former pseudo transparency code])
else
AC_MSG_RESULT([no])
fi

dnl New frontend driver
enableval=
AC_MSG_CHECKING([for frontend shell driver])
AC_ARG_ENABLE(frontend-shell-driver,
[  --disable-frontend-shell-driver
                          Do not invoke the frontend shell-based driver 
                          for adesklets [default=no]])
: ${enableval:=yes}
if test x$enableval != "xyes" ; then
AM_CONDITIONAL(FRONTEND_SHELL_SUPPORT, test 1 -eq 0)
AC_MSG_RESULT([no])
else
AC_DEFINE(FRONTEND_SHELL_DRIVER, 1,
[Enable the frontend shell driver mecanism])
AM_CONDITIONAL(FRONTEND_SHELL_SUPPORT, test 1 -eq 1)
AC_MSG_RESULT([yes])
fi

dnl Collect various system informations strings,
dnl for embedding into final executable
EXISTS=
WELCOME1="\"$PACKAGE $VERSION"
AC_CHECK_PROG(EXISTS,date,yes)
if test x$EXISTS = "xyes"; then
   WELCOME1="$WELCOME1 ("`date | sed 's/@<:@@<:@:space:@:>@@:>@\+/ /g'`")"
else
   WELCOME1="$WELCOME1 (unknown time)"
fi
EXISTS=
AC_CHECK_PROG(EXISTS,uname,yes)
if test x$EXISTS = "xyes"; then
   if uname -rs &> /dev/null; then
      WELCOME1="$WELCOME1, on "`uname -rs`"\""
   else
      WELCOME1="$WELCOME1, on an unknown OS\""
   fi
else
   WELCOME1="$WELCOME1, on an unknown OS\""
fi
AC_DEFINE_UNQUOTED(WELCOME_LINE_1, $WELCOME1, [Welcome message, first line.])
dnl Note: @<:@ and @:>@ are respective quadrigraphs for `[' and `]'
${CC} --version &> /dev/null && \
      WELCOME2="\"${CC} "`${CC} --version | sed -n '1 s/^@<:@^0-9@:>@*//p'`"\""
: ${WELCOME2:=\"${CC} unknown version\"}
AC_DEFINE_UNQUOTED(WELCOME_LINE_2, $WELCOME2, [Welcome message, second line.])

dnl Check for math
AC_CHECK_LIB(m, floor, HaveLibMath=yes)
if test x$HaveLibMath = "xyes" ; then
   LIBS="$LIBS -lm"
fi

dnl Variables save: all changes to CFLAGS, LIBS and LDFLAGS beyond
dnl this point will be wiped
TMP_CFLAGS="$CFLAGS"
TMP_LIBS="$LIBS"
TMP_LDFLAGS="$LDFLAGS"

dnl Check for readline
AC_CHECK_LIB(ncurses, tputs, HaveLibTermcap=yes; LibTermcap=ncurses,
   AC_CHECK_LIB(termcap, tputs, HaveLibTermcap=yes; LibTermcap=termcap,
      AC_CHECK_LIB(curses, tputs, HaveLibTermcap=yes; LibTermcap=curses,
                   HaveLibTermcap=no)))
if test $HaveLibTermcap = "yes" ; then
   LIBS="$LIBS -l$LibTermcap"
else
   AC_MSG_ERROR([Could not find terminal management library for readline
(either ncurses, termcap or curses).])
fi
AC_CHECK_LIB(readline,readline,
  READLINE_LIBS="-l$LibTermcap -lreadline",
  AC_MSG_ERROR([Could not find the readline library]),
  $LFLAGS)
LIBS="$TMP_LIBS"

dnl Check for libhistory support
withval=
AC_ARG_WITH(history,
[  --without-history       remove history support in interactive use
                          of the interpreter])
: ${withval:=yes}
if test x$withval = "xyes"; then
AC_CHECK_LIB(history,add_history,
	READLINE_LIBS="$READLINE_LIBS -lhistory";
	HaveLibHistory=history,
	AC_CHECK_LIB(readline,add_history,
	HaveLibHistory=readline,
	HaveLibHistory=no,
	$READLINE_LIBS),
	$READLINE_LIBS)

if test $HaveLibHistory = "no"; then
AC_MSG_WARN([Could not find GNU history library in the system])
else
AC_DEFINE(HAVE_READLINE_HISTORY_H,1,
	[Define to 1 if you have the <readline/history.h> header file.])
AC_CHECK_LIB($HaveLibHistory,free_history_entry,
	AC_DEFINE(HAVE_FREE_HISTORY_ENTRY,1,
	[Define to 1 if you have the `free_history_entry' function.]),,
	$READLINE_LIBS)
fi

AC_MSG_CHECKING([for history max command lenght])
enableval=
AC_ARG_ENABLE(history-max-command-lenght,
[  --enable-history-max-command-lenght=LEN
                          maximum string lenght (in characters) that a single
                          command can have and still be kept in history 
                          (a lenght of zero meaning no limits) [default=256]])
: ${enableval:=256}

if test $enableval -ge 0 ; then
AC_MSG_RESULT([yes])
AC_DEFINE_UNQUOTED(HISTORY_MAX_COMMAND_LENGHT,$enableval,
[maximum history string lenght.])
else
AC_MSG_RESULT([no])
AC_MSG_ERROR([history max command lenght must be positive or null])
fi

fi
AC_SUBST(READLINE_LIBS)	

dnl Check for 8 bit characters support forcing
enableval=
AC_ARG_ENABLE(force-extended-characters-input,
[  --enable-force-extended-characters-input
                          force input and ouput of unescaped 
			  height bits characters through GNU readline, 
			  regardless of the inputrc settings [default=no]])
: ${enableval:=no}
if ! test x$enableval = "xno" ; then
AC_DEFINE(FORCE_EXTENDED_CHARACTERS_INPUT,1,
[force 8 bit characters support in GNU readline])
fi

dnl Check for fontconfig
withval=
AC_ARG_WITH(fontconfig,
[  --without-fontconfig    remove fontconfig support enabling automatic 
                          detection of all truetype fonts already available
                          on the machine])
: ${withval:=yes}
if test x$withval = "xyes"; then
AC_PATH_PROG(PKG_CONFIG, pkg-config)
if ! test x$PKG_CONFIG = "x" ; then
   if $PKG_CONFIG --exists fontconfig 2> /dev/null; then
      FONTCONFIG_LIBS=`pkg-config --libs fontconfig`
      FONTCONFIG_CFLAGS=`pkg-config --cflags fontconfig`
   fi
fi
if test x$FONTCONFIG_LIBS = "x"; then
   FONTCONFIG_LIBS=-lfontconfig
fi
LIBS="$LIBS $FONTCONFIG_LIBS"
CFLAGS="$CFLAGS $FONTCONFIG_CFLAGS"
AC_MSG_CHECKING([for fontconfig])
AC_TRY_LINK(
#include <fontconfig/fontconfig.h>
, FcInit(),
AC_MSG_RESULT([yes])
AC_DEFINE(HAVE_FONTCONFIG_FONTCONFIG_H,1,[Define to 1 if you have the <fontconfig/fontconfig.h> header file.])

AC_MSG_CHECKING([For fontconfig FcFini()])
AC_TRY_LINK(
#include <fontconfig/fontconfig.h>
, FcFini(),
AC_MSG_RESULT([yes])
AC_DEFINE(HAVE_FONTCONFIG_FCFINI,1,[Define to 1 if FcFini() call exists])
,
AC_MSG_RESULT([no]))

,
AC_MSG_RESULT([no])
FONTCONFIG_LIBS=
FONTCONFIG_CFLAGS=
AC_MSG_WARN([
-----------------------------------------------------
`fontconfig' was not found on your system. 
Although `adesklets' will work anyway system-wide 
automatic font detection will not occur: 
it is therefore quite possible that only the 
default font provided with the package will display.
-----------------------------------------------------]))
AC_SUBST(FONTCONFIG_LIBS)
AC_SUBST(FONTCONFIG_CFLAGS)
LIBS="$TMP_LIBS"
CFLAGS="$TMP_CFLAGS"
fi

dnl Check for fork() system call
AC_CHECK_FUNCS(fork,,
AC_MSG_ERROR([Could not find the fork() system call]))

dnl Check for X headers and libraries
AC_PATH_X

if test x$no_x != "xyes"; then

if test x$x_includes != "x" ; then
   X_CFLAGS="-I$x_includes"
   CFLAGS="$CFLAGS $X_CFLAGS"	
fi

if test x$x_libraries != "x" ; then
   X_LIBS="-L$x_libraries"
   LDFLAGS="$LDFLAGS $X_LIBS"
fi

dnl Check for required functions in -lX11
AC_CHECK_LIB(X11, XOpenDisplay,
  X_LIBS="$X_LIBS -lX11"
  LIBS="$LIBS -lX11",
  AC_MSG_ERROR([Could not find XOpenDisplay in -lX11.])
)
AC_DEFINE(X_DISPLAY_SUPPORT,1,[Define to 1 if the X Window System is supported, 0 otherwise.])		      
else
AC_DEFINE(X_DISPLAY_SUPPORT,0,[Define to 1 if the X Window System is supported, 0 otherwise.])
   X_CFLAGS=
   X_LIBS=
AC_DEFINE(X_DISPLAY_MISSING,,[Defined if the X headers were not found])
fi
AC_SUBST(X_CFLAGS)
AC_SUBST(X_LIBS)

dnl Imlib2 detection
withval=
AC_ARG_WITH(imlib2,
        [  --with-imlib2=DIR       use imlib2 in <DIR>],
        [CFLAGS="$CFLAGS -I$withval/include"
        LIBS="-L$withval/lib $LIBS"])

AC_PATH_GENERIC_MODIFIED(imlib2, 1.1.2,,
  AC_MSG_ERROR([Cannot find imlib2: Is imlib2-config in the path?]),
  1.2.0,,
  AC_MSG_WARN([
-----------------------------------------------------
$PACKAGE should work with your version of imlib2...
But for better results you are _warmly_ encouraged 
to try out version 1.2.0 or above;
known bugs are still lurking in dark corners 
with what you have installed: beware strange problems 
with dynamic image filters for instance!  
-----------------------------------------------------]))

LIBS="$LIBS -lImlib2"
dnl CFLAGS="$CFLAGS $IMLIB2_CFLAGS"

AC_MSG_CHECKING([for imlib2 program linking with ad-hoc flags])
AC_TRY_LINK(
#ifndef X_DISPLAY_MISSING
	#include <X11/Xlib.h>
	#include <X11/Xutil.h>
	#include <X11/Xos.h>
	#include <X11/Xatom.h>
#endif	
	#include <Imlib2.h>
,	Imlib_Image image;
	image = imlib_load_image("toto.png");
#ifndef X_DISPLAY_MISSING
	imlib_context_set_display(NULL);
#endif
,
	AC_MSG_RESULT([yes])
	IMLIB2_CFLAGS=""
	IMLIB2_LIBS="-lImlib2",
	AC_MSG_RESULT([no])
	CFLAGS="$TMP_CFLAGS $IMLIB2_CFLAGS"
	LIBS="$TMP_LIBS $IMLIB2_LIBS"
	AC_MSG_CHECKING([for imlib2 program linking])
	AC_TRY_LINK(
#ifndef X_DISPLAY_MISSING
	#include <X11/Xlib.h>
	#include <X11/Xutil.h>
	#include <X11/Xos.h>
	#include <X11/Xatom.h>
#endif	
	#include <Imlib2.h>
,	Imlib_Image image;
	image = imlib_load_image("toto.png");
#ifndef X_DISPLAY_MISSING
	imlib_context_set_display(NULL);
#endif
,
	AC_MSG_RESULT([yes]),
	AC_MSG_RESULT([no])
	AC_MSG_ERROR([Cannot link Imlib2 program.
If you specified you did not want X support this can be caused
by your Imlib2 installment being configured so it needs it
or conversely. In that case reinstall Imlib2 with proper
--enable-x11-support parameter (as from enlightement CVS)
before retrying to configure this package.])))

AC_SUBST(IMLIB2_CFLAGS)
AC_SUBST(IMLIB2_LIBS)


dnl Test for uint typedef
AC_MSG_CHECKING([for uint typedef in standard library])
AC_TRY_COMPILE(
#ifdef HAVE_SYS_TYPES_H
	#include <sys/types.h>
#endif
#ifdef HAVE_UNISTD_H
	#include <unistd.h>
#endif
,	uint i,
	AC_MSG_RESULT([yes]),
	AC_MSG_RESULT([no])
	AC_DEFINE(UINT_NOT_DEFINED,1,
		  [Define to 1 if compability type uint is not detected.])	
)

dnl Important variables reset
CFLAGS="$TMP_CFLAGS"
LIBS="$TMP_LIBS"
LDFLAGS="$TMP_LDFLAGS"

dnl PKGDATADIR definition
dnl PKGDATADIR is only well constructed in make context,
dnl so let's put this AFTER all tests.
CFLAGS="$CFLAGS"' -DPKGDATADIR=\"$(pkgdatadir)\"'

dnl Output files
AM_CONFIG_HEADER(src/config.h)
AC_OUTPUT(Makefile
src/Makefile
data/Makefile
utils/Makefile
doc/Makefile
doc/adesklets_checkin.1
doc/adesklets_submit.1
doc/htmldoc.sh
doc/imlib2/Makefile
scripting/Makefile
scripting/protoize.sh
scripting/python/Makefile
scripting/python/config.h
scripting/python/setup.py
scripting/perl/Makefile
scripting/perl/Makefile.PL
scripting/perl/updateproto.pl
utils/adesklets_frontend.sh
)

dnl Change script(s) permissions
chmod +x scripting/protoize.sh scripting/perl/updateproto.pl \
      doc/htmldoc.sh utils/adesklets_frontend.sh

dnl Final warning, if needed
if test x$DEBUG = "xyes"; then
AC_MSG_WARN([
-----------------------------------------------------
you choosed to make a `debug' compilation: this will
generate a significantly bigger executable
that produces on stderr (by default) a lot of outputs 
unwanted during normal use; set up the ADESKLETS_LOG 
environment variable to a filename the interpreter 
can safely suffix with a pid related extension
than truncate to collect all debug outputs 
there instead. Be also aware that all optimisation 
settings (-O flags in CFLAGS) are discarded when 
enabling this.
-----------------------------------------------------])
fi
if test x$no_x = "xyes"; then
AC_MSG_WARN([
-----------------------------------------------------
you choosed to perform an `X Window less' build - 
which means you will eventually end up with 
a strictly command line program free of all
dependencies on xlib. If it is not what you intended 
please reconfigure. For this to work you do need first
to configure your Imlib2 incantation for not using
X Window itself.
-----------------------------------------------------
])
fi

if test x$PYTHON != "x" ; then
if test x$SIGPROC != "xyes" ; then
AC_MSG_WARN([
-----------------------------------------------------
your system does not seem to support reliable POSIX
signal calls such as `sigprocmask', `sigpending' or
`sigsuspend'. Although this does not affect $PACKAGE
directly, it seriously impedes python ability
to handle things well through its package. 
To our knowledge, no modern UNIX platforms lack 
those system calls...
-----------------------------------------------------
])
fi
fi
