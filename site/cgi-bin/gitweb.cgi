#! /usr/bin/env python
# Sylvain Fourmanoit <syfou@users.sourceforge.net>, 2007.
#
# Add automated redirection from the former gitweb interface url to the new, so that external links
# we cannot changes don't broke.
#
import cgi
import os

headers="""Status:  301 Moved Permanantly
Location: http://repo.or.cz/w/adesklets.git"""
qs='QUERY_STRING'

if qs in os.environ and len(os.environ[qs])>0:
    print '%s?%s\n' % (headers, os.environ[qs])
else:
    print '%s\n' % headers
