#
# phpBB Backup Script
# Dump of tables for adesklets
#
# DATE : 26-02-2005 09:53:17 GMT
#
#
# TABLE: phpbb_auth_access
#
DROP TABLE IF EXISTS phpbb_auth_access;
CREATE TABLE phpbb_auth_access(
	group_id mediumint(8) NOT NULL,
	forum_id smallint(5) unsigned NOT NULL,
	auth_view tinyint(1) NOT NULL,
	auth_read tinyint(1) NOT NULL,
	auth_post tinyint(1) NOT NULL,
	auth_reply tinyint(1) NOT NULL,
	auth_edit tinyint(1) NOT NULL,
	auth_delete tinyint(1) NOT NULL,
	auth_sticky tinyint(1) NOT NULL,
	auth_announce tinyint(1) NOT NULL,
	auth_vote tinyint(1) NOT NULL,
	auth_pollcreate tinyint(1) NOT NULL,
	auth_attachments tinyint(1) NOT NULL,
	auth_mod tinyint(1) NOT NULL, 
	KEY group_id (group_id), 
	KEY forum_id (forum_id)
);

#
# Table Data for phpbb_auth_access
#

INSERT INTO phpbb_auth_access (group_id, forum_id, auth_view, auth_read, auth_post, auth_reply, auth_edit, auth_delete, auth_sticky, auth_announce, auth_vote, auth_pollcreate, auth_attachments, auth_mod) VALUES('7', '6', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1');
INSERT INTO phpbb_auth_access (group_id, forum_id, auth_view, auth_read, auth_post, auth_reply, auth_edit, auth_delete, auth_sticky, auth_announce, auth_vote, auth_pollcreate, auth_attachments, auth_mod) VALUES('7', '7', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1');
INSERT INTO phpbb_auth_access (group_id, forum_id, auth_view, auth_read, auth_post, auth_reply, auth_edit, auth_delete, auth_sticky, auth_announce, auth_vote, auth_pollcreate, auth_attachments, auth_mod) VALUES('8', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1');
INSERT INTO phpbb_auth_access (group_id, forum_id, auth_view, auth_read, auth_post, auth_reply, auth_edit, auth_delete, auth_sticky, auth_announce, auth_vote, auth_pollcreate, auth_attachments, auth_mod) VALUES('8', '4', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1');
INSERT INTO phpbb_auth_access (group_id, forum_id, auth_view, auth_read, auth_post, auth_reply, auth_edit, auth_delete, auth_sticky, auth_announce, auth_vote, auth_pollcreate, auth_attachments, auth_mod) VALUES('8', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1');
INSERT INTO phpbb_auth_access (group_id, forum_id, auth_view, auth_read, auth_post, auth_reply, auth_edit, auth_delete, auth_sticky, auth_announce, auth_vote, auth_pollcreate, auth_attachments, auth_mod) VALUES('8', '3', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1');
INSERT INTO phpbb_auth_access (group_id, forum_id, auth_view, auth_read, auth_post, auth_reply, auth_edit, auth_delete, auth_sticky, auth_announce, auth_vote, auth_pollcreate, auth_attachments, auth_mod) VALUES('7', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1');
INSERT INTO phpbb_auth_access (group_id, forum_id, auth_view, auth_read, auth_post, auth_reply, auth_edit, auth_delete, auth_sticky, auth_announce, auth_vote, auth_pollcreate, auth_attachments, auth_mod) VALUES('8', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1');
#
# TABLE: phpbb_banlist
#
DROP TABLE IF EXISTS phpbb_banlist;
CREATE TABLE phpbb_banlist(
	ban_id mediumint(8) unsigned NOT NULL auto_increment,
	ban_userid mediumint(8) NOT NULL,
	ban_ip varchar(8) NOT NULL,
	ban_email varchar(255), 
	PRIMARY KEY (ban_id), 
	KEY ban_ip_user_id (ban_ip, ban_userid)
);
#
# TABLE: phpbb_categories
#
DROP TABLE IF EXISTS phpbb_categories;
CREATE TABLE phpbb_categories(
	cat_id mediumint(8) unsigned NOT NULL auto_increment,
	cat_title varchar(100),
	cat_order mediumint(8) unsigned NOT NULL, 
	PRIMARY KEY (cat_id), 
	KEY cat_order (cat_order)
);

#
# Table Data for phpbb_categories
#

INSERT INTO phpbb_categories (cat_id, cat_title, cat_order) VALUES('3', 'Configuration/Compilation/Installation problems', '10');
INSERT INTO phpbb_categories (cat_id, cat_title, cat_order) VALUES('4', 'Requests', '20');
INSERT INTO phpbb_categories (cat_id, cat_title, cat_order) VALUES('7', 'Miccelaneous', '30');
#
# TABLE: phpbb_config
#
DROP TABLE IF EXISTS phpbb_config;
CREATE TABLE phpbb_config(
	config_name varchar(255) NOT NULL,
	config_value varchar(255) NOT NULL, 
	PRIMARY KEY (config_name)
);

#
# Table Data for phpbb_config
#

INSERT INTO phpbb_config (config_name, config_value) VALUES('config_id', '1');
INSERT INTO phpbb_config (config_name, config_value) VALUES('board_disable', '0');
INSERT INTO phpbb_config (config_name, config_value) VALUES('sitename', 'http://adesklets.sf.net/forum');
INSERT INTO phpbb_config (config_name, config_value) VALUES('site_desc', 'adesklets community forum');
INSERT INTO phpbb_config (config_name, config_value) VALUES('cookie_name', 'phpbb2mysql');
INSERT INTO phpbb_config (config_name, config_value) VALUES('cookie_path', '/');
INSERT INTO phpbb_config (config_name, config_value) VALUES('cookie_domain', '');
INSERT INTO phpbb_config (config_name, config_value) VALUES('cookie_secure', '0');
INSERT INTO phpbb_config (config_name, config_value) VALUES('session_length', '3600');
INSERT INTO phpbb_config (config_name, config_value) VALUES('allow_html', '0');
INSERT INTO phpbb_config (config_name, config_value) VALUES('allow_html_tags', 'b,i,u,pre');
INSERT INTO phpbb_config (config_name, config_value) VALUES('allow_bbcode', '1');
INSERT INTO phpbb_config (config_name, config_value) VALUES('allow_smilies', '1');
INSERT INTO phpbb_config (config_name, config_value) VALUES('allow_sig', '1');
INSERT INTO phpbb_config (config_name, config_value) VALUES('allow_namechange', '0');
INSERT INTO phpbb_config (config_name, config_value) VALUES('allow_theme_create', '0');
INSERT INTO phpbb_config (config_name, config_value) VALUES('allow_avatar_local', '0');
INSERT INTO phpbb_config (config_name, config_value) VALUES('allow_avatar_remote', '0');
INSERT INTO phpbb_config (config_name, config_value) VALUES('allow_avatar_upload', '0');
INSERT INTO phpbb_config (config_name, config_value) VALUES('enable_confirm', '1');
INSERT INTO phpbb_config (config_name, config_value) VALUES('override_user_style', '0');
INSERT INTO phpbb_config (config_name, config_value) VALUES('posts_per_page', '15');
INSERT INTO phpbb_config (config_name, config_value) VALUES('topics_per_page', '50');
INSERT INTO phpbb_config (config_name, config_value) VALUES('hot_threshold', '25');
INSERT INTO phpbb_config (config_name, config_value) VALUES('max_poll_options', '10');
INSERT INTO phpbb_config (config_name, config_value) VALUES('max_sig_chars', '255');
INSERT INTO phpbb_config (config_name, config_value) VALUES('max_inbox_privmsgs', '10');
INSERT INTO phpbb_config (config_name, config_value) VALUES('max_sentbox_privmsgs', '5');
INSERT INTO phpbb_config (config_name, config_value) VALUES('max_savebox_privmsgs', '5');
INSERT INTO phpbb_config (config_name, config_value) VALUES('board_email_sig', 'Sylvain Fourmanoit');
INSERT INTO phpbb_config (config_name, config_value) VALUES('board_email', 'syfou@users.sourceforge.net');
INSERT INTO phpbb_config (config_name, config_value) VALUES('smtp_delivery', '1');
INSERT INTO phpbb_config (config_name, config_value) VALUES('smtp_host', 'mail.sourceforge.net');
INSERT INTO phpbb_config (config_name, config_value) VALUES('smtp_username', '');
INSERT INTO phpbb_config (config_name, config_value) VALUES('smtp_password', '');
INSERT INTO phpbb_config (config_name, config_value) VALUES('sendmail_fix', '0');
INSERT INTO phpbb_config (config_name, config_value) VALUES('require_activation', '1');
INSERT INTO phpbb_config (config_name, config_value) VALUES('flood_interval', '15');
INSERT INTO phpbb_config (config_name, config_value) VALUES('board_email_form', '0');
INSERT INTO phpbb_config (config_name, config_value) VALUES('avatar_filesize', '6144');
INSERT INTO phpbb_config (config_name, config_value) VALUES('avatar_max_width', '80');
INSERT INTO phpbb_config (config_name, config_value) VALUES('avatar_max_height', '80');
INSERT INTO phpbb_config (config_name, config_value) VALUES('avatar_path', 'images/avatars');
INSERT INTO phpbb_config (config_name, config_value) VALUES('avatar_gallery_path', 'images/avatars/gallery');
INSERT INTO phpbb_config (config_name, config_value) VALUES('smilies_path', 'images/smiles');
INSERT INTO phpbb_config (config_name, config_value) VALUES('default_style', '1');
INSERT INTO phpbb_config (config_name, config_value) VALUES('default_dateformat', 'D M d, Y g:i a');
INSERT INTO phpbb_config (config_name, config_value) VALUES('board_timezone', '0');
INSERT INTO phpbb_config (config_name, config_value) VALUES('prune_enable', '1');
INSERT INTO phpbb_config (config_name, config_value) VALUES('privmsg_disable', '0');
INSERT INTO phpbb_config (config_name, config_value) VALUES('gzip_compress', '0');
INSERT INTO phpbb_config (config_name, config_value) VALUES('coppa_fax', '');
INSERT INTO phpbb_config (config_name, config_value) VALUES('coppa_mail', '');
INSERT INTO phpbb_config (config_name, config_value) VALUES('record_online_users', '1');
INSERT INTO phpbb_config (config_name, config_value) VALUES('record_online_date', '1109384140');
INSERT INTO phpbb_config (config_name, config_value) VALUES('server_name', 'adesklets.sourceforge.net');
INSERT INTO phpbb_config (config_name, config_value) VALUES('server_port', '80');
INSERT INTO phpbb_config (config_name, config_value) VALUES('script_path', '/forum/');
INSERT INTO phpbb_config (config_name, config_value) VALUES('version', '.0.12');
INSERT INTO phpbb_config (config_name, config_value) VALUES('board_startdate', '1109384077');
INSERT INTO phpbb_config (config_name, config_value) VALUES('default_lang', 'english');
#
# TABLE: phpbb_disallow
#
DROP TABLE IF EXISTS phpbb_disallow;
CREATE TABLE phpbb_disallow(
	disallow_id mediumint(8) unsigned NOT NULL auto_increment,
	disallow_username varchar(25) NOT NULL, 
	PRIMARY KEY (disallow_id)
);
#
# TABLE: phpbb_forums
#
DROP TABLE IF EXISTS phpbb_forums;
CREATE TABLE phpbb_forums(
	forum_id smallint(5) unsigned NOT NULL,
	cat_id mediumint(8) unsigned NOT NULL,
	forum_name varchar(150),
	forum_desc text,
	forum_status tinyint(4) NOT NULL,
	forum_order mediumint(8) unsigned DEFAULT '1' NOT NULL,
	forum_posts mediumint(8) unsigned NOT NULL,
	forum_topics mediumint(8) unsigned NOT NULL,
	forum_last_post_id mediumint(8) unsigned NOT NULL,
	prune_next int(11),
	prune_enable tinyint(1) NOT NULL,
	auth_view tinyint(2) NOT NULL,
	auth_read tinyint(2) NOT NULL,
	auth_post tinyint(2) NOT NULL,
	auth_reply tinyint(2) NOT NULL,
	auth_edit tinyint(2) NOT NULL,
	auth_delete tinyint(2) NOT NULL,
	auth_sticky tinyint(2) NOT NULL,
	auth_announce tinyint(2) NOT NULL,
	auth_vote tinyint(2) NOT NULL,
	auth_pollcreate tinyint(2) NOT NULL,
	auth_attachments tinyint(2) NOT NULL, 
	PRIMARY KEY (forum_id), 
	KEY forums_order (forum_order), 
	KEY cat_id (cat_id), 
	KEY forum_last_post_id (forum_last_post_id)
);

#
# Table Data for phpbb_forums
#

INSERT INTO phpbb_forums (forum_id, cat_id, forum_name, forum_desc, forum_status, forum_order, forum_posts, forum_topics, forum_last_post_id, prune_next, prune_enable, auth_view, auth_read, auth_post, auth_reply, auth_edit, auth_delete, auth_sticky, auth_announce, auth_vote, auth_pollcreate, auth_attachments) VALUES('1', '3', 'So it doesn\'t configure right', 'Post here if you have configuration problems.', '0', '10', '0', '0', '0', NULL, '0', '0', '0', '1', '1', '1', '1', '3', '3', '1', '1', '0');
INSERT INTO phpbb_forums (forum_id, cat_id, forum_name, forum_desc, forum_status, forum_order, forum_posts, forum_topics, forum_last_post_id, prune_next, prune_enable, auth_view, auth_read, auth_post, auth_reply, auth_edit, auth_delete, auth_sticky, auth_announce, auth_vote, auth_pollcreate, auth_attachments) VALUES('2', '3', 'So it doesn\'t compile', 'Post here if you get compilation errors.', '0', '20', '0', '0', '0', NULL, '0', '0', '0', '1', '1', '1', '1', '3', '3', '1', '1', '0');
INSERT INTO phpbb_forums (forum_id, cat_id, forum_name, forum_desc, forum_status, forum_order, forum_posts, forum_topics, forum_last_post_id, prune_next, prune_enable, auth_view, auth_read, auth_post, auth_reply, auth_edit, auth_delete, auth_sticky, auth_announce, auth_vote, auth_pollcreate, auth_attachments) VALUES('3', '3', 'So it installs, but there are still issues', 'Post here if install completed, but you cannot use a single deskle anyway, including test/test.py.', '0', '30', '0', '0', '0', NULL, '0', '0', '0', '1', '1', '1', '1', '3', '3', '1', '1', '0');
INSERT INTO phpbb_forums (forum_id, cat_id, forum_name, forum_desc, forum_status, forum_order, forum_posts, forum_topics, forum_last_post_id, prune_next, prune_enable, auth_view, auth_read, auth_post, auth_reply, auth_edit, auth_delete, auth_sticky, auth_announce, auth_vote, auth_pollcreate, auth_attachments) VALUES('4', '4', 'About adesklets (core)', 'Post requests for adesklets core interpreter.', '0', '10', '0', '0', '0', NULL, '0', '0', '0', '1', '1', '1', '1', '3', '3', '1', '1', '0');
INSERT INTO phpbb_forums (forum_id, cat_id, forum_name, forum_desc, forum_status, forum_order, forum_posts, forum_topics, forum_last_post_id, prune_next, prune_enable, auth_view, auth_read, auth_post, auth_reply, auth_edit, auth_delete, auth_sticky, auth_announce, auth_vote, auth_pollcreate, auth_attachments) VALUES('5', '4', 'About existing desklets', 'Post features request (bug fixes or others) concerning already existing desklets.', '0', '20', '0', '0', '0', NULL, '0', '0', '0', '1', '1', '1', '1', '3', '3', '1', '1', '0');
INSERT INTO phpbb_forums (forum_id, cat_id, forum_name, forum_desc, forum_status, forum_order, forum_posts, forum_topics, forum_last_post_id, prune_next, prune_enable, auth_view, auth_read, auth_post, auth_reply, auth_edit, auth_delete, auth_sticky, auth_announce, auth_vote, auth_pollcreate, auth_attachments) VALUES('6', '4', 'About new desklets', 'Post here you ideas of what desklets you think would be nice.', '0', '30', '0', '0', '0', NULL, '0', '0', '0', '1', '1', '1', '1', '3', '3', '1', '1', '0');
INSERT INTO phpbb_forums (forum_id, cat_id, forum_name, forum_desc, forum_status, forum_order, forum_posts, forum_topics, forum_last_post_id, prune_next, prune_enable, auth_view, auth_read, auth_post, auth_reply, auth_edit, auth_delete, auth_sticky, auth_announce, auth_vote, auth_pollcreate, auth_attachments) VALUES('7', '3', 'So there is a problem with a specific desklet', 'Post here your issues with specific desklets.', '0', '40', '0', '0', '0', NULL, '0', '0', '0', '1', '1', '1', '1', '3', '3', '1', '1', '0');
INSERT INTO phpbb_forums (forum_id, cat_id, forum_name, forum_desc, forum_status, forum_order, forum_posts, forum_topics, forum_last_post_id, prune_next, prune_enable, auth_view, auth_read, auth_post, auth_reply, auth_edit, auth_delete, auth_sticky, auth_announce, auth_vote, auth_pollcreate, auth_attachments) VALUES('8', '7', 'Screenshots', 'Post your screenshots here.', '0', '20', '1', '1', '2', NULL, '0', '0', '0', '3', '0', '1', '1', '3', '3', '1', '3', '0');
INSERT INTO phpbb_forums (forum_id, cat_id, forum_name, forum_desc, forum_status, forum_order, forum_posts, forum_topics, forum_last_post_id, prune_next, prune_enable, auth_view, auth_read, auth_post, auth_reply, auth_edit, auth_delete, auth_sticky, auth_announce, auth_vote, auth_pollcreate, auth_attachments) VALUES('9', '7', 'Off the wall', 'Post here everthing you couldn\'t post anywhere else.', '0', '30', '0', '0', '0', NULL, '0', '0', '0', '1', '1', '1', '1', '3', '3', '1', '1', '0');
INSERT INTO phpbb_forums (forum_id, cat_id, forum_name, forum_desc, forum_status, forum_order, forum_posts, forum_topics, forum_last_post_id, prune_next, prune_enable, auth_view, auth_read, auth_post, auth_reply, auth_edit, auth_delete, auth_sticky, auth_announce, auth_vote, auth_pollcreate, auth_attachments) VALUES('10', '7', 'Announcements', 'Special announcements', '0', '10', '0', '0', '0', NULL, '0', '0', '0', '3', '3', '3', '3', '3', '3', '1', '3', '0');
#
# TABLE: phpbb_forum_prune
#
DROP TABLE IF EXISTS phpbb_forum_prune;
CREATE TABLE phpbb_forum_prune(
	prune_id mediumint(8) unsigned NOT NULL auto_increment,
	forum_id smallint(5) unsigned NOT NULL,
	prune_days smallint(5) unsigned NOT NULL,
	prune_freq smallint(5) unsigned NOT NULL, 
	PRIMARY KEY (prune_id), 
	KEY forum_id (forum_id)
);
#
# TABLE: phpbb_groups
#
DROP TABLE IF EXISTS phpbb_groups;
CREATE TABLE phpbb_groups(
	group_id mediumint(8) NOT NULL auto_increment,
	group_type tinyint(4) DEFAULT '1' NOT NULL,
	group_name varchar(40) NOT NULL,
	group_description varchar(255) NOT NULL,
	group_moderator mediumint(8) NOT NULL,
	group_single_user tinyint(1) DEFAULT '1' NOT NULL, 
	PRIMARY KEY (group_id), 
	KEY group_single_user (group_single_user)
);

#
# Table Data for phpbb_groups
#

INSERT INTO phpbb_groups (group_id, group_type, group_name, group_description, group_moderator, group_single_user) VALUES('1', '1', 'Anonymous', 'Personal User', '0', '1');
INSERT INTO phpbb_groups (group_id, group_type, group_name, group_description, group_moderator, group_single_user) VALUES('2', '1', 'Admin', 'Personal User', '0', '1');
INSERT INTO phpbb_groups (group_id, group_type, group_name, group_description, group_moderator, group_single_user) VALUES('7', '0', 'Desklet author', 'Anyone who currently supports at least one working desklet', '2', '0');
INSERT INTO phpbb_groups (group_id, group_type, group_name, group_description, group_moderator, group_single_user) VALUES('6', '1', '', 'Personal User', '0', '1');
INSERT INTO phpbb_groups (group_id, group_type, group_name, group_description, group_moderator, group_single_user) VALUES('8', '0', 'Core developer', 'Anyone who actively works on adesklets core interpreter package', '2', '0');
#
# TABLE: phpbb_posts
#
DROP TABLE IF EXISTS phpbb_posts;
CREATE TABLE phpbb_posts(
	post_id mediumint(8) unsigned NOT NULL auto_increment,
	topic_id mediumint(8) unsigned NOT NULL,
	forum_id smallint(5) unsigned NOT NULL,
	poster_id mediumint(8) NOT NULL,
	post_time int(11) NOT NULL,
	poster_ip varchar(8) NOT NULL,
	post_username varchar(25),
	enable_bbcode tinyint(1) DEFAULT '1' NOT NULL,
	enable_html tinyint(1) NOT NULL,
	enable_smilies tinyint(1) DEFAULT '1' NOT NULL,
	enable_sig tinyint(1) DEFAULT '1' NOT NULL,
	post_edit_time int(11),
	post_edit_count smallint(5) unsigned NOT NULL, 
	PRIMARY KEY (post_id), 
	KEY forum_id (forum_id), 
	KEY topic_id (topic_id), 
	KEY poster_id (poster_id), 
	KEY post_time (post_time)
);

#
# Table Data for phpbb_posts
#

INSERT INTO phpbb_posts (post_id, topic_id, forum_id, poster_id, post_time, poster_ip, post_username, enable_bbcode, enable_html, enable_smilies, enable_sig, post_edit_time, post_edit_count) VALUES('2', '2', '8', '2', '1109408814', '45466282', '', '1', '0', '1', '0', NULL, '0');
#
# TABLE: phpbb_posts_text
#
DROP TABLE IF EXISTS phpbb_posts_text;
CREATE TABLE phpbb_posts_text(
	post_id mediumint(8) unsigned NOT NULL,
	bbcode_uid varchar(10) NOT NULL,
	post_subject varchar(60),
	post_text text, 
	PRIMARY KEY (post_id)
);

#
# Table Data for phpbb_posts_text
#

INSERT INTO phpbb_posts_text (post_id, bbcode_uid, post_subject, post_text) VALUES('2', '5e25cdd980', 'Screenshots for the user gallery', 'Have any good adesklets screenshots you want to share with the world? Post them here!');
#
# TABLE: phpbb_privmsgs
#
DROP TABLE IF EXISTS phpbb_privmsgs;
CREATE TABLE phpbb_privmsgs(
	privmsgs_id mediumint(8) unsigned NOT NULL auto_increment,
	privmsgs_type tinyint(4) NOT NULL,
	privmsgs_subject varchar(255) NOT NULL,
	privmsgs_from_userid mediumint(8) NOT NULL,
	privmsgs_to_userid mediumint(8) NOT NULL,
	privmsgs_date int(11) NOT NULL,
	privmsgs_ip varchar(8) NOT NULL,
	privmsgs_enable_bbcode tinyint(1) DEFAULT '1' NOT NULL,
	privmsgs_enable_html tinyint(1) NOT NULL,
	privmsgs_enable_smilies tinyint(1) DEFAULT '1' NOT NULL,
	privmsgs_attach_sig tinyint(1) DEFAULT '1' NOT NULL, 
	PRIMARY KEY (privmsgs_id), 
	KEY privmsgs_from_userid (privmsgs_from_userid), 
	KEY privmsgs_to_userid (privmsgs_to_userid)
);
#
# TABLE: phpbb_privmsgs_text
#
DROP TABLE IF EXISTS phpbb_privmsgs_text;
CREATE TABLE phpbb_privmsgs_text(
	privmsgs_text_id mediumint(8) unsigned NOT NULL,
	privmsgs_bbcode_uid varchar(10) NOT NULL,
	privmsgs_text text, 
	PRIMARY KEY (privmsgs_text_id)
);
#
# TABLE: phpbb_ranks
#
DROP TABLE IF EXISTS phpbb_ranks;
CREATE TABLE phpbb_ranks(
	rank_id smallint(5) unsigned NOT NULL auto_increment,
	rank_title varchar(50) NOT NULL,
	rank_min mediumint(8) NOT NULL,
	rank_special tinyint(1),
	rank_image varchar(255), 
	PRIMARY KEY (rank_id)
);

#
# Table Data for phpbb_ranks
#

INSERT INTO phpbb_ranks (rank_id, rank_title, rank_min, rank_special, rank_image) VALUES('1', 'Site Admin', '-1', '1', NULL);
#
# TABLE: phpbb_search_results
#
DROP TABLE IF EXISTS phpbb_search_results;
CREATE TABLE phpbb_search_results(
	search_id int(11) unsigned NOT NULL,
	session_id varchar(32) NOT NULL,
	search_array text NOT NULL, 
	PRIMARY KEY (search_id), 
	KEY session_id (session_id)
);

#
# Table Data for phpbb_search_results
#

INSERT INTO phpbb_search_results (search_id, session_id, search_array) VALUES('822981272', 'bf2dc97b90e90211e0d4686d2204efc6', 'a:7:{s:14:\"search_results\";s:1:\"2\";s:17:\"total_match_count\";i:1;s:12:\"split_search\";N;s:7:\"sort_by\";i:0;s:8:\"sort_dir\";s:4:\"DESC\";s:12:\"show_results\";s:5:\"posts\";s:12:\"return_chars\";i:200;}');
#
# TABLE: phpbb_search_wordlist
#
DROP TABLE IF EXISTS phpbb_search_wordlist;
CREATE TABLE phpbb_search_wordlist(
	word_text varchar(50) binary NOT NULL,
	word_id mediumint(8) unsigned NOT NULL auto_increment,
	word_common tinyint(1) unsigned NOT NULL, 
	PRIMARY KEY (word_text), 
	KEY word_id (word_id)
);

#
# Table Data for phpbb_search_wordlist
#

INSERT INTO phpbb_search_wordlist (word_text, word_id, word_common) VALUES('phpbb', '3', '0');
INSERT INTO phpbb_search_wordlist (word_text, word_id, word_common) VALUES('user', '24', '0');
INSERT INTO phpbb_search_wordlist (word_text, word_id, word_common) VALUES('share', '23', '0');
INSERT INTO phpbb_search_wordlist (word_text, word_id, word_common) VALUES('screenshots', '16', '0');
INSERT INTO phpbb_search_wordlist (word_text, word_id, word_common) VALUES('post', '22', '0');
INSERT INTO phpbb_search_wordlist (word_text, word_id, word_common) VALUES('gallery', '21', '0');
INSERT INTO phpbb_search_wordlist (word_text, word_id, word_common) VALUES('adesklets', '20', '0');
#
# TABLE: phpbb_search_wordmatch
#
DROP TABLE IF EXISTS phpbb_search_wordmatch;
CREATE TABLE phpbb_search_wordmatch(
	post_id mediumint(8) unsigned NOT NULL,
	word_id mediumint(8) unsigned NOT NULL,
	title_match tinyint(1) NOT NULL, 
	KEY post_id (post_id), 
	KEY word_id (word_id)
);

#
# Table Data for phpbb_search_wordmatch
#

INSERT INTO phpbb_search_wordmatch (post_id, word_id, title_match) VALUES('2', '24', '1');
INSERT INTO phpbb_search_wordmatch (post_id, word_id, title_match) VALUES('2', '16', '1');
INSERT INTO phpbb_search_wordmatch (post_id, word_id, title_match) VALUES('2', '21', '1');
INSERT INTO phpbb_search_wordmatch (post_id, word_id, title_match) VALUES('2', '20', '0');
INSERT INTO phpbb_search_wordmatch (post_id, word_id, title_match) VALUES('2', '22', '0');
INSERT INTO phpbb_search_wordmatch (post_id, word_id, title_match) VALUES('2', '16', '0');
INSERT INTO phpbb_search_wordmatch (post_id, word_id, title_match) VALUES('2', '23', '0');
#
# TABLE: phpbb_sessions
#
DROP TABLE IF EXISTS phpbb_sessions;
CREATE TABLE phpbb_sessions(
	session_id char(32) NOT NULL,
	session_user_id mediumint(8) NOT NULL,
	session_start int(11) NOT NULL,
	session_time int(11) NOT NULL,
	session_ip char(8) NOT NULL,
	session_page int(11) NOT NULL,
	session_logged_in tinyint(1) NOT NULL, 
	PRIMARY KEY (session_id), 
	KEY session_user_id (session_user_id), 
	KEY session_id_ip_user_id (session_id, session_ip, session_user_id)
);

#
# Table Data for phpbb_sessions
#

INSERT INTO phpbb_sessions (session_id, session_user_id, session_start, session_time, session_ip, session_page, session_logged_in) VALUES('bf2dc97b90e90211e0d4686d2204efc6', '2', '1109409305', '1109411545', '45466282', '0', '1');
#
# TABLE: phpbb_smilies
#
DROP TABLE IF EXISTS phpbb_smilies;
CREATE TABLE phpbb_smilies(
	smilies_id smallint(5) unsigned NOT NULL auto_increment,
	code varchar(50),
	smile_url varchar(100),
	emoticon varchar(75), 
	PRIMARY KEY (smilies_id)
);

#
# Table Data for phpbb_smilies
#

INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('1', ':D', 'icon_biggrin.gif', 'Very Happy');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('2', ':-D', 'icon_biggrin.gif', 'Very Happy');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('3', ':grin:', 'icon_biggrin.gif', 'Very Happy');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('4', ':)', 'icon_smile.gif', 'Smile');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('5', ':-)', 'icon_smile.gif', 'Smile');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('6', ':smile:', 'icon_smile.gif', 'Smile');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('7', ':(', 'icon_sad.gif', 'Sad');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('8', ':-(', 'icon_sad.gif', 'Sad');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('9', ':sad:', 'icon_sad.gif', 'Sad');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('10', ':o', 'icon_surprised.gif', 'Surprised');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('11', ':-o', 'icon_surprised.gif', 'Surprised');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('12', ':eek:', 'icon_surprised.gif', 'Surprised');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('13', ':shock:', 'icon_eek.gif', 'Shocked');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('14', ':?', 'icon_confused.gif', 'Confused');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('15', ':-?', 'icon_confused.gif', 'Confused');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('16', ':???:', 'icon_confused.gif', 'Confused');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('17', '8)', 'icon_cool.gif', 'Cool');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('18', '8-)', 'icon_cool.gif', 'Cool');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('19', ':cool:', 'icon_cool.gif', 'Cool');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('20', ':lol:', 'icon_lol.gif', 'Laughing');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('21', ':x', 'icon_mad.gif', 'Mad');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('22', ':-x', 'icon_mad.gif', 'Mad');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('23', ':mad:', 'icon_mad.gif', 'Mad');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('24', ':P', 'icon_razz.gif', 'Razz');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('25', ':-P', 'icon_razz.gif', 'Razz');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('26', ':razz:', 'icon_razz.gif', 'Razz');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('27', ':oops:', 'icon_redface.gif', 'Embarassed');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('28', ':cry:', 'icon_cry.gif', 'Crying or Very sad');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('29', ':evil:', 'icon_evil.gif', 'Evil or Very Mad');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('30', ':twisted:', 'icon_twisted.gif', 'Twisted Evil');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('31', ':roll:', 'icon_rolleyes.gif', 'Rolling Eyes');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('32', ':wink:', 'icon_wink.gif', 'Wink');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('33', ';)', 'icon_wink.gif', 'Wink');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('34', ';-)', 'icon_wink.gif', 'Wink');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('35', ':!:', 'icon_exclaim.gif', 'Exclamation');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('36', ':?:', 'icon_question.gif', 'Question');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('37', ':idea:', 'icon_idea.gif', 'Idea');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('38', ':arrow:', 'icon_arrow.gif', 'Arrow');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('39', ':|', 'icon_neutral.gif', 'Neutral');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('40', ':-|', 'icon_neutral.gif', 'Neutral');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('41', ':neutral:', 'icon_neutral.gif', 'Neutral');
INSERT INTO phpbb_smilies (smilies_id, code, smile_url, emoticon) VALUES('42', ':mrgreen:', 'icon_mrgreen.gif', 'Mr. Green');
#
# TABLE: phpbb_themes
#
DROP TABLE IF EXISTS phpbb_themes;
CREATE TABLE phpbb_themes(
	themes_id mediumint(8) unsigned NOT NULL auto_increment,
	template_name varchar(30) NOT NULL,
	style_name varchar(30) NOT NULL,
	head_stylesheet varchar(100),
	body_background varchar(100),
	body_bgcolor varchar(6),
	body_text varchar(6),
	body_link varchar(6),
	body_vlink varchar(6),
	body_alink varchar(6),
	body_hlink varchar(6),
	tr_color1 varchar(6),
	tr_color2 varchar(6),
	tr_color3 varchar(6),
	tr_class1 varchar(25),
	tr_class2 varchar(25),
	tr_class3 varchar(25),
	th_color1 varchar(6),
	th_color2 varchar(6),
	th_color3 varchar(6),
	th_class1 varchar(25),
	th_class2 varchar(25),
	th_class3 varchar(25),
	td_color1 varchar(6),
	td_color2 varchar(6),
	td_color3 varchar(6),
	td_class1 varchar(25),
	td_class2 varchar(25),
	td_class3 varchar(25),
	fontface1 varchar(50),
	fontface2 varchar(50),
	fontface3 varchar(50),
	fontsize1 tinyint(4),
	fontsize2 tinyint(4),
	fontsize3 tinyint(4),
	fontcolor1 varchar(6),
	fontcolor2 varchar(6),
	fontcolor3 varchar(6),
	span_class1 varchar(25),
	span_class2 varchar(25),
	span_class3 varchar(25),
	img_size_poll smallint(5) unsigned,
	img_size_privmsg smallint(5) unsigned, 
	PRIMARY KEY (themes_id)
);

#
# Table Data for phpbb_themes
#

INSERT INTO phpbb_themes (themes_id, template_name, style_name, head_stylesheet, body_background, body_bgcolor, body_text, body_link, body_vlink, body_alink, body_hlink, tr_color1, tr_color2, tr_color3, tr_class1, tr_class2, tr_class3, th_color1, th_color2, th_color3, th_class1, th_class2, th_class3, td_color1, td_color2, td_color3, td_class1, td_class2, td_class3, fontface1, fontface2, fontface3, fontsize1, fontsize2, fontsize3, fontcolor1, fontcolor2, fontcolor3, span_class1, span_class2, span_class3, img_size_poll, img_size_privmsg) VALUES('1', 'subSilver', 'subSilver', 'subSilver.css', '', 'E5E5E5', '000000', '006699', '5493B4', '', 'DD6900', 'EFEFEF', 'DEE3E7', 'D1D7DC', '', '', '', '98AAB1', '006699', 'FFFFFF', 'cellpic1.gif', 'cellpic3.gif', 'cellpic2.jpg', 'FAFAFA', 'FFFFFF', '', 'row1', 'row2', '', 'Verdana, Arial, Helvetica, sans-serif', 'Trebuchet MS', 'Courier, \'Courier New\', sans-serif', '10', '11', '12', '444444', '006600', 'FFA34F', '', '', '', NULL, NULL);
#
# TABLE: phpbb_themes_name
#
DROP TABLE IF EXISTS phpbb_themes_name;
CREATE TABLE phpbb_themes_name(
	themes_id smallint(5) unsigned NOT NULL,
	tr_color1_name char(50),
	tr_color2_name char(50),
	tr_color3_name char(50),
	tr_class1_name char(50),
	tr_class2_name char(50),
	tr_class3_name char(50),
	th_color1_name char(50),
	th_color2_name char(50),
	th_color3_name char(50),
	th_class1_name char(50),
	th_class2_name char(50),
	th_class3_name char(50),
	td_color1_name char(50),
	td_color2_name char(50),
	td_color3_name char(50),
	td_class1_name char(50),
	td_class2_name char(50),
	td_class3_name char(50),
	fontface1_name char(50),
	fontface2_name char(50),
	fontface3_name char(50),
	fontsize1_name char(50),
	fontsize2_name char(50),
	fontsize3_name char(50),
	fontcolor1_name char(50),
	fontcolor2_name char(50),
	fontcolor3_name char(50),
	span_class1_name char(50),
	span_class2_name char(50),
	span_class3_name char(50), 
	PRIMARY KEY (themes_id)
);

#
# Table Data for phpbb_themes_name
#

INSERT INTO phpbb_themes_name (themes_id, tr_color1_name, tr_color2_name, tr_color3_name, tr_class1_name, tr_class2_name, tr_class3_name, th_color1_name, th_color2_name, th_color3_name, th_class1_name, th_class2_name, th_class3_name, td_color1_name, td_color2_name, td_color3_name, td_class1_name, td_class2_name, td_class3_name, fontface1_name, fontface2_name, fontface3_name, fontsize1_name, fontsize2_name, fontsize3_name, fontcolor1_name, fontcolor2_name, fontcolor3_name, span_class1_name, span_class2_name, span_class3_name) VALUES('1', 'The lightest row colour', 'The medium row color', 'The darkest row colour', '', '', '', 'Border round the whole page', 'Outer table border', 'Inner table border', 'Silver gradient picture', 'Blue gradient picture', 'Fade-out gradient on index', 'Background for quote boxes', 'All white areas', '', 'Background for topic posts', '2nd background for topic posts', '', 'Main fonts', 'Additional topic title font', 'Form fonts', 'Smallest font size', 'Medium font size', 'Normal font size (post body etc)', 'Quote & copyright text', 'Code text colour', 'Main table header text colour', '', '', '');
#
# TABLE: phpbb_topics
#
DROP TABLE IF EXISTS phpbb_topics;
CREATE TABLE phpbb_topics(
	topic_id mediumint(8) unsigned NOT NULL auto_increment,
	forum_id smallint(8) unsigned NOT NULL,
	topic_title char(60) NOT NULL,
	topic_poster mediumint(8) NOT NULL,
	topic_time int(11) NOT NULL,
	topic_views mediumint(8) unsigned NOT NULL,
	topic_replies mediumint(8) unsigned NOT NULL,
	topic_status tinyint(3) NOT NULL,
	topic_vote tinyint(1) NOT NULL,
	topic_type tinyint(3) NOT NULL,
	topic_first_post_id mediumint(8) unsigned NOT NULL,
	topic_last_post_id mediumint(8) unsigned NOT NULL,
	topic_moved_id mediumint(8) unsigned NOT NULL, 
	PRIMARY KEY (topic_id), 
	KEY forum_id (forum_id), 
	KEY topic_moved_id (topic_moved_id), 
	KEY topic_status (topic_status), 
	KEY topic_type (topic_type)
);

#
# Table Data for phpbb_topics
#

INSERT INTO phpbb_topics (topic_id, forum_id, topic_title, topic_poster, topic_time, topic_views, topic_replies, topic_status, topic_vote, topic_type, topic_first_post_id, topic_last_post_id, topic_moved_id) VALUES('2', '8', 'Screenshots for the user gallery', '2', '1109408814', '3', '0', '0', '0', '1', '2', '2', '0');
#
# TABLE: phpbb_topics_watch
#
DROP TABLE IF EXISTS phpbb_topics_watch;
CREATE TABLE phpbb_topics_watch(
	topic_id mediumint(8) unsigned NOT NULL,
	user_id mediumint(8) NOT NULL,
	notify_status tinyint(1) NOT NULL, 
	KEY topic_id (topic_id), 
	KEY user_id (user_id), 
	KEY notify_status (notify_status)
);
#
# TABLE: phpbb_user_group
#
DROP TABLE IF EXISTS phpbb_user_group;
CREATE TABLE phpbb_user_group(
	group_id mediumint(8) NOT NULL,
	user_id mediumint(8) NOT NULL,
	user_pending tinyint(1), 
	KEY group_id (group_id), 
	KEY user_id (user_id)
);

#
# Table Data for phpbb_user_group
#

INSERT INTO phpbb_user_group (group_id, user_id, user_pending) VALUES('1', '-1', '0');
INSERT INTO phpbb_user_group (group_id, user_id, user_pending) VALUES('2', '2', '0');
INSERT INTO phpbb_user_group (group_id, user_id, user_pending) VALUES('7', '2', '0');
INSERT INTO phpbb_user_group (group_id, user_id, user_pending) VALUES('8', '2', '0');
INSERT INTO phpbb_user_group (group_id, user_id, user_pending) VALUES('6', '3', '0');
#
# TABLE: phpbb_users
#
DROP TABLE IF EXISTS phpbb_users;
CREATE TABLE phpbb_users(
	user_id mediumint(8) NOT NULL,
	user_active tinyint(1) DEFAULT '1',
	username varchar(25) NOT NULL,
	user_password varchar(32) NOT NULL,
	user_session_time int(11) NOT NULL,
	user_session_page smallint(5) NOT NULL,
	user_lastvisit int(11) NOT NULL,
	user_regdate int(11) NOT NULL,
	user_level tinyint(4),
	user_posts mediumint(8) unsigned NOT NULL,
	user_timezone decimal(5,2) DEFAULT '0.00' NOT NULL,
	user_style tinyint(4),
	user_lang varchar(255),
	user_dateformat varchar(14) DEFAULT 'd M Y H:i' NOT NULL,
	user_new_privmsg smallint(5) unsigned NOT NULL,
	user_unread_privmsg smallint(5) unsigned NOT NULL,
	user_last_privmsg int(11) NOT NULL,
	user_emailtime int(11),
	user_viewemail tinyint(1),
	user_attachsig tinyint(1),
	user_allowhtml tinyint(1) DEFAULT '1',
	user_allowbbcode tinyint(1) DEFAULT '1',
	user_allowsmile tinyint(1) DEFAULT '1',
	user_allowavatar tinyint(1) DEFAULT '1' NOT NULL,
	user_allow_pm tinyint(1) DEFAULT '1' NOT NULL,
	user_allow_viewonline tinyint(1) DEFAULT '1' NOT NULL,
	user_notify tinyint(1) DEFAULT '1' NOT NULL,
	user_notify_pm tinyint(1) NOT NULL,
	user_popup_pm tinyint(1) NOT NULL,
	user_rank int(11),
	user_avatar varchar(100),
	user_avatar_type tinyint(4) NOT NULL,
	user_email varchar(255),
	user_icq varchar(15),
	user_website varchar(100),
	user_from varchar(100),
	user_sig text,
	user_sig_bbcode_uid varchar(10),
	user_aim varchar(255),
	user_yim varchar(255),
	user_msnm varchar(255),
	user_occ varchar(100),
	user_interests varchar(255),
	user_actkey varchar(32),
	user_newpasswd varchar(32), 
	PRIMARY KEY (user_id), 
	KEY user_session_time (user_session_time)
);

#
# Table Data for phpbb_users
#

INSERT INTO phpbb_users (user_id, user_active, username, user_password, user_session_time, user_session_page, user_lastvisit, user_regdate, user_level, user_posts, user_timezone, user_style, user_lang, user_dateformat, user_new_privmsg, user_unread_privmsg, user_last_privmsg, user_emailtime, user_viewemail, user_attachsig, user_allowhtml, user_allowbbcode, user_allowsmile, user_allowavatar, user_allow_pm, user_allow_viewonline, user_notify, user_notify_pm, user_popup_pm, user_rank, user_avatar, user_avatar_type, user_email, user_icq, user_website, user_from, user_sig, user_sig_bbcode_uid, user_aim, user_yim, user_msnm, user_occ, user_interests, user_actkey, user_newpasswd) VALUES('-1', '0', 'Anonymous', '', '0', '0', '0', '1109384077', '0', '0', '0.00', NULL, '', '', '0', '0', '0', NULL, '0', '0', '0', '1', '1', '1', '0', '1', '0', '1', '0', NULL, '', '0', '', '', '', '', '', NULL, '', '', '', '', '', '', '');
INSERT INTO phpbb_users (user_id, user_active, username, user_password, user_session_time, user_session_page, user_lastvisit, user_regdate, user_level, user_posts, user_timezone, user_style, user_lang, user_dateformat, user_new_privmsg, user_unread_privmsg, user_last_privmsg, user_emailtime, user_viewemail, user_attachsig, user_allowhtml, user_allowbbcode, user_allowsmile, user_allowavatar, user_allow_pm, user_allow_viewonline, user_notify, user_notify_pm, user_popup_pm, user_rank, user_avatar, user_avatar_type, user_email, user_icq, user_website, user_from, user_sig, user_sig_bbcode_uid, user_aim, user_yim, user_msnm, user_occ, user_interests, user_actkey, user_newpasswd) VALUES('2', '1', 'syfou', 'f4bcc1bebd2aef8d19655376828e01f7', '1109411545', '0', '1109408848', '1109384077', '1', '2', '-5.00', '1', 'english', 'd M Y h:i a', '0', '0', '1109404825', NULL, '1', '1', '0', '1', '0', '1', '1', '0', '0', '1', '0', '1', '', '0', 'syfou@users.sourceforge.net', '', '', 'Montreal, Canada', '--
Sylvain', '64bd76ace8', '', '', '', '', '', '', '');
INSERT INTO phpbb_users (user_id, user_active, username, user_password, user_session_time, user_session_page, user_lastvisit, user_regdate, user_level, user_posts, user_timezone, user_style, user_lang, user_dateformat, user_new_privmsg, user_unread_privmsg, user_last_privmsg, user_emailtime, user_viewemail, user_attachsig, user_allowhtml, user_allowbbcode, user_allowsmile, user_allowavatar, user_allow_pm, user_allow_viewonline, user_notify, user_notify_pm, user_popup_pm, user_rank, user_avatar, user_avatar_type, user_email, user_icq, user_website, user_from, user_sig, user_sig_bbcode_uid, user_aim, user_yim, user_msnm, user_occ, user_interests, user_actkey, user_newpasswd) VALUES('3', '1', 'test', 'f4bcc1bebd2aef8d19655376828e01f7', '1109409041', '1', '1109408132', '1109388049', '0', '0', '0.00', '1', 'english', 'D M d, Y g:i a', '0', '0', '0', NULL, '0', '1', '0', '1', '1', '1', '1', '1', '0', '1', '1', '0', '', '0', 'sourceforge@fourmanoit.ca', '', '', '', '', '', '', '', '', '', '', '', NULL);
#
# TABLE: phpbb_vote_desc
#
DROP TABLE IF EXISTS phpbb_vote_desc;
CREATE TABLE phpbb_vote_desc(
	vote_id mediumint(8) unsigned NOT NULL auto_increment,
	topic_id mediumint(8) unsigned NOT NULL,
	vote_text text NOT NULL,
	vote_start int(11) NOT NULL,
	vote_length int(11) NOT NULL, 
	PRIMARY KEY (vote_id), 
	KEY topic_id (topic_id)
);
#
# TABLE: phpbb_vote_results
#
DROP TABLE IF EXISTS phpbb_vote_results;
CREATE TABLE phpbb_vote_results(
	vote_id mediumint(8) unsigned NOT NULL,
	vote_option_id tinyint(4) unsigned NOT NULL,
	vote_option_text varchar(255) NOT NULL,
	vote_result int(11) NOT NULL, 
	KEY vote_option_id (vote_option_id), 
	KEY vote_id (vote_id)
);
#
# TABLE: phpbb_vote_voters
#
DROP TABLE IF EXISTS phpbb_vote_voters;
CREATE TABLE phpbb_vote_voters(
	vote_id mediumint(8) unsigned NOT NULL,
	vote_user_id mediumint(8) NOT NULL,
	vote_user_ip char(8) NOT NULL, 
	KEY vote_id (vote_id), 
	KEY vote_user_id (vote_user_id), 
	KEY vote_user_ip (vote_user_ip)
);
#
# TABLE: phpbb_words
#
DROP TABLE IF EXISTS phpbb_words;
CREATE TABLE phpbb_words(
	word_id mediumint(8) unsigned NOT NULL auto_increment,
	word char(100) NOT NULL,
	replacement char(100) NOT NULL, 
	PRIMARY KEY (word_id)
);
#
# TABLE: phpbb_confirm
#
DROP TABLE IF EXISTS phpbb_confirm;
CREATE TABLE phpbb_confirm(
	confirm_id char(32) NOT NULL,
	session_id char(32) NOT NULL,
	code char(6) NOT NULL, 
	PRIMARY KEY (session_id, confirm_id)
);

#
# Table Data for phpbb_confirm
#

INSERT INTO phpbb_confirm (confirm_id, session_id, code) VALUES('2ca193e71d76aacdf0b6535f7bc65523', '4386de58ac6e2ef562ae0fe1c4f9aa16', 'GXDTEJ');
INSERT INTO phpbb_confirm (confirm_id, session_id, code) VALUES('3588976e60618e84bd539ca071707a8d', '4386de58ac6e2ef562ae0fe1c4f9aa16', 'I5Z58B');
