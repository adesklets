#! /usr/bin/env python
"""
Exporter for old adesklets phpbb 2.2.x forums db.
Written By Sylvain Fourmanoit <syfou@users.sourceforge.net>, 2007.

This exporter reads a phpbb 2.2 forum description from a SQL database, and dumps
it in a static, html form. The output is basically compatible with xhtml strict
spec: process it with htmltidy to have the final corrections made.

That's not a perfect exporter, but it does a 'good enough' job
in this case... It shouldn't be hard to adapt anyway.
"""
from __future__ import with_statement

from sqlobject import *
import logging, sys, os, os.path, time, re

#------------------------------------------------------------------------------
# A couple global variables
#
DB = 'mysql://user:passwd@host/db_name?charset=utf8'
DestDir = 'adesklets_forum_archive'

#------------------------------------------------------------------------------
# Start by establishing the default connection
#
sqlhub.processConnection = connectionForURI(DB)

#------------------------------------------------------------------------------
# The various first degree class
#
class PhpbbUsers(SQLObject):
   class sqlmeta:
      fromDatabase = True
      idName = 'user_id'

class PhpbbRanks(SQLObject):
   class sqlmeta:
      fromDatabase = True
      idName = 'rank_id'

class PhpbbCategories(SQLObject):
   class sqlmeta:
      fromDatabase = True
      idName = 'cat_id'
        
class PhpbbForums(SQLObject):
   class sqlmeta:
      fromDatabase = True
      idName = 'forum_id'

class PhpbbTopics(SQLObject):
   class sqlmeta:
      fromDatabase = True
      idName = 'topic_id'
        
class PhpbbPosts(SQLObject):
   class sqlmeta:
      fromDatabase = True
      idName = 'post_id'
        
class PhpbbPostsText(SQLObject):
   class sqlmeta:
      fromDatabase = True
      idName = 'post_id'

#------------------------------------------------------------------------------
# Helper class to generate html
#
class HTMLFile:
   """
   Commodity boilerplate for html generation. Example:
   
   with HTMLFile('path/to/basename', 'title') as f:
   print >> f, '<p>Hello!</p>'
   """
   _base = DestDir
   _debug = False
   
   def __init__(self, basename, title=None, header='', symlink=None):
      # Build the filename, and make sure the directory exists
      filename = os.path.join(self._base, basename + '.html')
      logging.info('generating %s' % filename)
      
      head, tail = os.path.split(filename)
      if not os.path.isdir(head): os.makedirs(head)

      # Now initialize the file
      if symlink is None:
         css = '/'.join(['..'] * (
            max(len(filename.split('/')) - 2, 0)) + ['reservoir.css'])
         self._f = file(os.path.join(self._base, basename + '.html'), 'w')
         if title is None: title = basename
         self.write('''<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=us-ascii" />
<meta name="generator" content="dump_phpbb.py: Sylvain little dumper" />
<title>%s</title>
<style type="text/css" media="all">
@import "%s"; <!-- This style sheet is adapted from Rob Chandanais work -->
</style>
%s</head>
<body>\n''' % (title, css, header))
      else:
         if os.path.exists(filename): os.unlink(filename)
         os.symlink(symlink, filename)
         
   def __enter__(self):
      return self
   
   def __exit__(self, *exc_info):
      self.close()
      
   def write(self, str):
      if self._debug:
         sys.stdout.write(str)
      self._f.write(str)
        
   def close(self):
      self.write('''<div><br /><hr />
<h2>adesklets is proud to be hosted on:</h2><p><a href=
"http://sourceforge.net/projects/adesklets/"><img src=
"http://sourceforge.net/sflogo.php?group_id=126227&amp;type=5"
width="210" height="62" alt="SourceForge.net Logo" /></a></p>
<p>Back to <a href="http://adesklets.sourceforge.net/">adesklets.sf.net</a>.</p>
</div>
</body>
</html>''')
      if self._debug:
         sys.stdout.flush()
      self._f.close()

#------------------------------------------------------------------------------
# Helper class to translate bbcode to html
#
class BBDecode:
   """BBcode translator"""
   _tags = ['url', 'code', 'quote', 'list', 'email', 'color', 'size',
            'u', 'b', 'i', '*']
   _list = []
   _incode = False
   _logurl = None
    
   def __init__(self):
      self._tag_match = re.compile('\[([^]]*)\]|\r\n')
      self._quote_match = re.compile('.*="(.*)"$')
      self._tags = self._tags + ['/%s' % tag for tag in self._tags]

      self._url_mangle_table = [
         (re.compile(regex), sub) for regex, sub in (
         ('adesklets\.sourceforge\.net\/forum', None),
         ('viewtopic\.php.*[?;]t=([0-9]+)', '../topics/%(m)s.html'),
         ('viewtopic\.php.*[?;]p=([0-9]+)', '../posts/%(m)s.html#%(m)s'),
         ('profile\.php.*[?;]u=([0-9]+)', '../users/%(m)s.html'),
         ('search\.php.*[?;]search_keywords=([^&]*)',
          ('http://www.google.com/search?q=%(m)s+' +
           'site%%3Aadesklets.sourceforge.net%%2fforum%%2f')),
         ('viewforum\.php.*[?;]f=([0-9]+)', '../forums/%(m)s.html'))]
        
   def __call__(self, post, text):
      if text is None: return '[corrupted text]' # Deal with DB corruption
      if not isinstance(text, type('')):
         raw = text.postText.tostring()
      else:
         raw = text

      return self._tag_match.sub(self._repl, raw)

   def _repl(self, expr):
      if expr.group(1) == None:
         return '<br />\n' if not self._incode else '\n'

      tag, arg = self._splittag(expr.group(1))
      if tag == 'url':
         if self._logurl is not None:
            print >> self._logurl, arg   
         return '<a href="%s">' % self._mangle_url(arg)
      elif tag == '/url':
         return '</a>'
      elif tag == 'code':
         self._incode = True
         return ('<div class="code"><div class="code_header">' +
                 '<h4>Code:</h4></div><pre><tt>')
      elif tag == '/code':
         self._incode = False
         return '</tt></pre></div>'
      elif tag == 'quote':
         m = self._quote_match.match(arg)                
         return ('<div class="quote">' +
                 '<div class="quote_header"><h4>%s</h4></div>') % \
                 ('%s wrote:' % m.group(1)
                  if m is not None else 'Quote:')
      elif tag == '/quote':
         return '</div>'
      elif tag == 'list':
         if arg == '1':
            self._list.append('</od')
            return '<ol>'
         else:
            self._list.append('</ul>')
            return '<ul>'
         return '<ol>' if arg == '1' else '<ul>'
      elif tag == '/list':
         return self._list.pop()
      elif tag == 'email' or tag == '/email':
         return ''
      elif tag == 'color':
         return '<span class="color_%s">' % arg.split(':')[0]
      elif tag == 'size':
         return '<span class="size_%s">' % arg.split(':')[0]
      elif tag == 'u':
         return '<span class="underline">'
      elif tag == 'b':
         return '<span class="bold">'
      elif tag == 'i':
         return '<span class="italic">'
      elif tag in ('/color', '/size', '/u', '/b', '/i'):
         return '</span>'
      elif tag == '*':
         return '<li />'
      else:
         return expr.group(0) # Unknown tag: just echo it.

   def _splittag(self, str):
      for lex in self._tags:
         if lex == str[:len(lex)]:
            return lex, str[len(lex)+1:]
      return None, None

   def _mangle_url(self, url):
      """Rewrite the self-referencing URL to match the new scheme"""
      if self._url_mangle_table[0][0].search(url):
         for regex, sub in self._url_mangle_table[1:]:
            m = regex.search(url)
            if m:
               return sub % {'m':m.group(1)}
         return 'http://example.org/dead/link'
      return url

bbdecode = BBDecode()

#------------------------------------------------------------------------------
# Ok, now we are ready to dump
#
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s')
#HTMLFile._debug = True
#BBDecode._logurl = file('log.txt', 'w')

# Dump main index
#
with HTMLFile('index', 'adesklets community forum archive') as f:
   print >> f, \
'''<h1>adesklets community forum archive</h1>

<p>Welcome to the adesklets community forum archive. Everything contributed
through the former <a href="http://phpbb.com/">phpBB</a> forum hosted on
adesklets site is still available here; a lot of valuable users\' Q&amp;A is
this way kept online for your convenience.</p>

<p>If you want to search the archive, the content is fully static: <a href=
"http://www.google.com/search?q=site:adesklets.sourceforge.net/forum_archive/"
>Google it</a> (when they will have indexed us), or <a href=
"http://prdownloads.sourceforge.net/adesklets/adesklets_forum_archive.tar.bz2?download"
>download it</a> (approx. 0.5 MB), then use your favorite local search tool
(grep, etc.).</p>
'''
   for cat in PhpbbCategories.select(orderBy = PhpbbCategories.q.catID):
      print >> f, '<h2>%s</h2>' % cat.catTitle
      forums = list(PhpbbForums.select(PhpbbForums.q.catID == cat.catID,
                                       orderBy = PhpbbForums.q.forumOrder))
      if len(forums) > 0:
         print >> f, '<ul>'
         for forum in forums:
            print >> f, ('<li><a href="forums/%d.html">%s</a> ' +
                         '(%s topics , %s posts)</li>') % \
                         (forum.forumID, forum.forumName,
                          forum.forumTopics, forum.forumPosts)
         print >> f, '</ul>'

#sys.exit()

# Dump secondary (forums) index
#
for forum in PhpbbForums.select():
   with HTMLFile('forums/%d' % forum.forumID, forum.forumName) as f:
      cat = list(PhpbbCategories.select(PhpbbCategories.q.catID ==
                                        forum.catID))[0]
      print >> f, ('<h2><a href="../index.html">Main index</a> ' +
                   '&gt; %s &gt; %s</h2>') % \
                     (cat.catTitle, forum.forumName)
      topics = list(PhpbbTopics.select(
         PhpbbTopics.q.forumID == forum.forumID,
         orderBy = PhpbbTopics.q.topicTime))
      if len(topics) > 0:
         print >> f, '<ul>'
         for topic in topics:
            poster = list(PhpbbUsers.select(PhpbbUsers.q.userID ==
                                            topic.topicPoster))[0]
            print >> f, (
               '<li><a href="../topics/%d.html">%s</a> ' + 
               '(by <a href="../users/%d.html">%s</a>, %d replies)</li>') \
               % (topic.topicID, topic.topicTitle, poster.userID,
                  poster.username, topic.topicReplies)
         print >> f, '</ul>'

# Dump ternary (topics) index... Which is also the content.
#
for topic in PhpbbTopics.select():
   with HTMLFile('topics/%d' % topic.topicID, topic.topicTitle) as f:
      # Generate the title, with links
      #
      forum = list(PhpbbForums.select(PhpbbForums.q.forumID ==
                                      topic.forumID))[0]
      print >> f, ('<h2><a href="../index.html">Main index</a> &gt; ' +
                   '<a href="../forums/%d.html">%s</a> &gt; %s</h2>') % \
                   (forum.forumID, forum.forumName, topic.topicTitle)

      # Then, output the posts
      #
      posts = list(PhpbbPosts.select(PhpbbPosts.q.topicID == topic.topicID,
                                       orderBy = PhpbbPosts.q.postTime))
      for i, post in enumerate(posts):
         poster = list(PhpbbUsers.select(PhpbbUsers.q.userID ==
                                         post.posterID))[0]
         rank   = list(PhpbbRanks.select(PhpbbRanks.q.rankID ==
                                         poster.userRank))
         rank   = rank[0].rankTitle if len(rank) > 0 else 'User'
         post_text = list(PhpbbPostsText.select(PhpbbPostsText.q.postID ==
                                                post.postID))
         post_text = post_text[0] if len(post_text) > 0 else None # Corruption!
         post_subject = post_text.postSubject if post_text else ''
                                          
         print >> f, \
'''<div class="%s">
<a name="%d"></a>
<div class="post_head">
<h3>By <a href="../users/%d.html">%s</a> (%s), on %s%s.</h3>
</div>
<div class="post_body">
%s
</div></div>''' % (('post_even' if i % 2 == 0 else 'post_odd'), post.postID, 
                   poster.userID, poster.username, rank,
                   (time.ctime(post.postTime) +
                    (', last edited on ' + time.ctime(post.postEditTime)
                     if post.postEditCount > 0 else '')),
                   ': %s' % post_subject if len(post_subject) > 0 else '',
                   bbdecode(post, post_text))

# Dump users description
#
for user in PhpbbUsers.select():
   posts = list(PhpbbPosts.select(PhpbbPosts.q.posterID == user.userID,
                                  orderBy = PhpbbPosts.q.postTime))
   if len(posts) > 0:
      rank = list(PhpbbRanks.select(PhpbbRanks.q.rankID ==
                                    user.userRank))
      rank = rank[0].rankTitle if len(rank) > 0 else 'regular user'
      
      with HTMLFile('users/%d' % user.userID, user.username) as f:
         print >> f, \
'''<h2><a href="../index.html">Main index</a> &gt; user %(name)s</h2>
<p>%(name)s is a %(rank)s%(place)s.</p>
%(website)s
<h3>There %(posts)s by %(name)s:</h3>''' % {
   'name': user.username,
   'rank': rank.lower(),
   'place': (', living in %s' % user.userFrom
             if len(user.userFrom) > 0 else ''),
   'website': (('<p>This user has a ' +
                '<a href="%s">home page</a>.</p>') %
               user.userWebsite
               if len(user.userWebsite) > 0 else ''),
   'posts': ('is a single post'
             if len(posts) == 1 else 'are %d posts' % len(posts))}

         print >> f, '<ol>'
         for post in reversed(posts):
            topic = list(PhpbbTopics.select(
               PhpbbTopics.q.topicID == post.topicID))[0]
            print >> f, (('<li />' +
                          'In <a href="../topics/%d.html#%d">%s</a>' +
                          ', on %s') %
                         (post.topicID, post.postID,
                          topic.topicTitle, time.ctime(post.postTime)))
         print >> f, '</ol>'

# Dump posts links (as symlinks) -- this is not directly useful to the end user,
# but it is for automated redirection using stub phpbb scripts.
#
for post in PhpbbPosts.select():
   HTMLFile('posts/%d' % post.postID,
            symlink='../topics/%d.html' % post.topicID)
