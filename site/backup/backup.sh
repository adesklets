#! /bin/sh
#----------------------------------------------------------------
# Written by S.Fourmanoit, 2005.
#
# Minimal non-interactive forum backup script.
#
# Use bzip2, mysqldump as well as RCS
# to keep around an incremental backup of 
# adesklets' phpBB forum content.
#
# Warning:
# 
# it has to be invoked interactively once, on new file creation.
#
#----------------------------------------------------------------
SQL='forum.sql'

#----------------------------------------------------------------
SQL_RCS="$SQL,v"
SQL_RCS_BZ2="$SQL,v.bz2"

#----------------------------------------------------------------
cd `dirname $0` || exit 1
test -f $SQL_RCS_BZ2 && bunzip2 $SQL_RCS_BZ2
test -f $SQL_RCS && co -l $SQL_RCS

mysqldump adesklets > $SQL && \
    ci -msg# $SQL && \
    bzip2 --best $SQL_RCS
