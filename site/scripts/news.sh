#! /bin/sh
cat<<EOF 
<page>
<title>adesklets news</title>
EOF
cat include/menu.xml
for FILE in $(find news -name '*.txt' -maxdepth 1 | sort -n | tac); do
    if ! head -n 1 ${FILE} | grep '^@' > /dev/null ; then
	sed -i "1i@$(date -u)@" ${FILE}
    fi	
    cat<<EOF
    <news date="$(sed -n '1{s/@\(.*\)@/\1/;p}' ${FILE})"
	  title="$(sed -n '2p' ${FILE})">
	$(sed -n '3,$p' ${FILE}) 
    </news>
EOF
done
echo '</page>'
