<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

   <!-- XSL stylesheet to convert adesklets xml source website to xhtml
    !   Written by S.Fourmanoit
    !-->

   <xsl:output method="html" encoding="utf8" />

   <!-- Root element !-->
   <xsl:template match="page">
      <html>
         <head>
	    <title>
	       <xsl:value-of select="title/text()"/>
	    </title>
	    <style type="text/css" media="all">@import "reservoir.css";</style>
	    <script src="http://www.google-analytics.com/urchin.js" 
		    type="text/javascript"></script>
	    <script type="text/javascript">
	      _uacct = "UA-70960-1";urchinTracker();
	    </script>
	    <xsl:if test="count(child::alternate)=1">
		<xsl:for-each select="child::alternate">
		   <xsl:element name="link">
			<xsl:attribute name="rel">alternate</xsl:attribute>
			<xsl:attribute name="type">
			   <xsl:value-of select="@type" />
			</xsl:attribute>
			<xsl:attribute name="href">
			   <xsl:value-of select="@href" />
			</xsl:attribute>
		   </xsl:element>
		</xsl:for-each>
	    </xsl:if>
	 </head>
         <body>
            <xsl:apply-templates select="*"/>

	    <!--
	    <xsl:value-of name="out" select="count(child::news)"/>
	    !-->

	    <xsl:if test="count(child::info)=1">
	       <xsl:for-each select="child::info">
	          <p id="Footer" class="footer">
	          The latest version is
		  <a href="http://freshmeat.net/projects/adesklets/">
		     <xsl:value-of select="@version" />
		  </a>, and there are <a href="desklets.html">
		  <xsl:value-of 
		     select="count(document('../desklets.xml')/page/desklet)"/>
		  desklets</a> listed online
		  (<xsl:value-of
		     select="@last_modified" />).
		  </p>
               </xsl:for-each>
	    </xsl:if>
         </body>
      </html>
   </xsl:template>

  <!-- Page title in place !-->
  <xsl:template match="title">
    <div id="Header">
      <h1><xsl:apply-templates /></h1>
    </div>
  </xsl:template>

  <!-- Left menu 
  	<menu>
		<a href=...
	</menu>
  !-->
  <xsl:template match="menu">
      <div id="Menu">
         <xsl:for-each select="a">
            <xsl:if test="@href">
               <xsl:element name="a">
	          <xsl:attribute name="href">
	             <xsl:value-of select="@href"/>
	          </xsl:attribute>
	          <xsl:apply-templates/>
               </xsl:element>
            </xsl:if>
            <br/>
         </xsl:for-each>
      </div>
      <div id="Logo">
         <h1>Proudly hosted on:</h1><p>
         <a href="http://sourceforge.net/projects/adesklets/">
	 <img src="http://sourceforge.net/sflogo.php?group_id=126227&amp;type=5" 
	    width="210" height="62" border="0" alt="SourceForge.net Logo" />
	 </a>
	 </p>
      </div>
  </xsl:template>

   <!-- Boxed item 
   	<item title="Some title">Some content</item>
   !-->
   <xsl:template match="item">
      <div class="content">
         <xsl:if test="@title">
            <h2>
               <xsl:value-of select="@title"/>
            </h2>
         </xsl:if>
         <xsl:apply-templates/>
      </div>
   </xsl:template>

   <!-- News item 
        <news date="some date" title="some title">Some content</news>
    !-->
   <xsl:template match="news">
      <div class="content">      
         <xsl:choose>
         <xsl:when test="@date">
	    <h3 class="minititle">
	       <xsl:value-of select="@date"/>
	       <xsl:if test="@title">
	          - <xsl:value-of select="@title"/>
	       </xsl:if>
	    </h3>
	 </xsl:when>
	 <xsl:otherwise>
	    <h3>
	       Unknown date
	       <xsl:if test="@title">
	          - <xsl:value-of select="@title"/>
	       </xsl:if>
	    </h3>
	 </xsl:otherwise>
	 </xsl:choose>
	 
         <xsl:apply-templates/>
      </div>
   </xsl:template>

   <!-- Desklet description item 
        <desklets name="name" version="version" lang="language"
	thumbnail="image_uri" screenshot="image_uri"
	download="download_uri">
		Description paragraph
	</desklets>

	If thumbnail is omitted, link to default values will be created
	(possible screenshot will ignored). Same thing for download.
    !-->
  <xsl:template match="desklet">
      <div class="content2">
         <h3 class="minititle">
	   <xsl:value-of select="@name"/>
	   <xsl:text> </xsl:text>
	   <xsl:value-of select="@version"/>
	 </h3>

	 <xsl:choose>
	 <xsl:when test="@thumbnail">
            <xsl:element name="a">
	       <xsl:attribute name="href">
	          <xsl:value-of select="@screenshot"/>
	       </xsl:attribute>
	       <xsl:element name="img">
	          <xsl:attribute name="class">thumbnail</xsl:attribute>
	          <xsl:attribute name="src">
	             <xsl:value-of select="@thumbnail"/>
	          </xsl:attribute>
	       </xsl:element>
	    </xsl:element>
	 </xsl:when>
	 <xsl:otherwise>
	    <xsl:element name="a">
	       <xsl:attribute name="href">
	          <xsl:text>images/</xsl:text>
	          <xsl:value-of select="@name" />
	          <xsl:text>_screen.jpg</xsl:text>
	       </xsl:attribute>
	       <xsl:element name="img">
	          <xsl:attribute name="class">thumbnail</xsl:attribute>
		  <xsl:attribute name="src">
		     <xsl:text>images/</xsl:text>
	             <xsl:value-of select="@name" />
	             <xsl:text>_thumb.jpg</xsl:text>
		  </xsl:attribute>
	       </xsl:element>
	    </xsl:element>
	 </xsl:otherwise>
	 </xsl:choose>

	 <span><strong>Language:</strong>
	 <xsl:choose>
	   
	   <xsl:when test="@lang">
	     <xsl:value-of select="@lang" />
	   </xsl:when>
	   <xsl:otherwise>
	     Python
	   </xsl:otherwise>
	 </xsl:choose>
	 </span>

         <xsl:apply-templates/>

	 <p>
         <xsl:choose> 
            <xsl:when test="@download">
	       <xsl:element name="a">
	          <xsl:attribute name="href">
		     <xsl:value-of select="@download" />
		  </xsl:attribute>
		  Download <xsl:value-of select="@name" />
	                   <xsl:text> </xsl:text>
	                   <xsl:value-of select="@version"/>
	       </xsl:element>
	    </xsl:when>
	    <xsl:otherwise>
	       <xsl:element name="a">
	          <xsl:attribute name="href">
		     <xsl:text>http://prdownloads.sourceforge.net/adesklets/</xsl:text>
		     <xsl:value-of select="@name" />
		     <xsl:text>-</xsl:text>
		     <xsl:value-of select="@version" />
		     <xsl:text>.tar.bz2?download</xsl:text>
		  </xsl:attribute>
	       Download <xsl:value-of select="@name" />
	                <xsl:text> </xsl:text>
                        <xsl:value-of select="@version"/>
	       </xsl:element>
	    </xsl:otherwise>
         </xsl:choose>

	 </p>
      </div>
   </xsl:template>

   <xsl:template match="author">
      <span>
      <strong>Author: </strong>

      <xsl:choose>
	<xsl:when test="@packager">
	  <xsl:value-of select="@name" /> 
	  <span>
	  <strong> Maintainer: </strong>
	  <xsl:element name="a">
	    <xsl:attribute name="href">
	      mailto:<xsl:value-of select="@email" />
	    </xsl:attribute>
	    <xsl:value-of select="@packager" />
	  </xsl:element>
	  </span>
	</xsl:when>
	<xsl:when test="@email">
	  <xsl:element name="a">
	    <xsl:attribute name="href">
	      mailto:<xsl:value-of select="@email" />
	    </xsl:attribute>
	    <xsl:value-of select="@name" />
	  </xsl:element>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:value-of select="@name" />
	</xsl:otherwise>
      </xsl:choose>
      </span><br />
   </xsl:template>

   <!-- Forward port of the HTML equivalent !-->
   <xsl:template match="a">
      <xsl:if test="@href">
      <xsl:element
         name="a">
	 <xsl:attribute name="href">
	    <xsl:value-of select="@href"/>
	 </xsl:attribute>
	 <xsl:apply-templates/>
      </xsl:element>
      </xsl:if>
   </xsl:template>

   <!-- Forward port of the HTML equivalent 
   	<img src="some_uri" nocolor="">
   !-->
   <xsl:template match="img">
      <xsl:element name="img">
         <xsl:attribute name="src">
	    <xsl:value-of select="@src"/>
	 </xsl:attribute>
	 <xsl:if test="@nocolor">
	    <xsl:attribute name="class">nocolor</xsl:attribute>
	 </xsl:if>
      </xsl:element>
   </xsl:template>

   <!-- Forward port of the HTML equivalent !-->
   <xsl:template match="p">
      <p><xsl:apply-templates/></p>
   </xsl:template>

   <xsl:template match="br">
     <br />
   </xsl:template>

   <xsl:template match="h2">
      <h2><xsl:apply-templates/></h2>
   </xsl:template>

   <xsl:template match="strong">
	<strong><xsl:apply-templates/></strong>
   </xsl:template>

   <xsl:template match="em">
	<em><xsl:apply-templates/></em>
   </xsl:template>
       
</xsl:stylesheet>