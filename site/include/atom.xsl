<?xml version="1.0"?>
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:date="http://exslt.org/dates-and-times"
	xmlns:html="http://www.w3.org/1999/xhtml"
	xmlns:atom="http://purl.org/atom/ns#"
	extension-element-prefixes="date"
	exclude-result-prefixes="date html atom">

   <!-- XSL stylesheet to syndicate desklets description
    !   into an atom feed (http://www.atomenabled.org/).
    !
    !   Painfully written by S.Fourmanoit out of very pertinents 
    !   advices from Aristotle Pagaltzis, whom the author also has to thanks
    !   for sample xlst fragments (http://plasmasturm.org/feeds/).
    !
    !   The atom feed contains one entry per desklet, with ID simply being
    !   based on its name, and title being its name and version. `Modified'
    !   date contains the date of latest update, while `Issued' date is the 
    !   date of first creation. Content is taken verbatim from source.
    !   
    !   Note 1 : for the "date refreshing" mechanism to work right, you need
    !            to keep around the former adesklets.atom feed in site/, 
    !            since it is parsed by this stylesheet.
    !
    !   Note 2 : this code works, but is pretty badly written: the previous
    !            'site/desklets.atom' feed is parsed as many times as
    !            there are desklets in the input document; if you know how
    !            to do this in a clean way, let me know.
    !-->

   <xsl:output method="xml" encoding="iso-8859-1"/>

   <!-- Root element !-->
   <xsl:template match="page">
      <xsl:variable name="now" select="date:date-time()"/>

      <feed version="0.3" xmlns="http://purl.org/atom/ns#">
      <title>adesklets' desklets</title>
      <modified>
         <xsl:value-of select="$now" />
      </modified>
      <author><name>adesklets community</name></author>
      <link rel="alternate" type="text/html" 
      href="http://adesklets.sf.net/desklets.html"/>

      <xsl:for-each select="desklet">
         <xsl:variable name="id">
	    <xsl:text>tag:adesklets.sf.net,2005:</xsl:text>
	    <xsl:value-of select="@name"/>
	 </xsl:variable>
	 <xsl:variable name="title">
	    <xsl:value-of select="@name"/>
	    <xsl:text> </xsl:text>
	    <xsl:value-of select="@version"/>
	 </xsl:variable>

         <entry>
            <id><xsl:value-of select="$id"/></id>
	    <xsl:element name="title"> 
	       <xsl:value-of select="$title" />
	    </xsl:element>

	    <xsl:choose>
	      <xsl:when test="./author/@packager">
		<author>
		  <name><xsl:value-of select="./author/@name"/></name>
		</author>
		<author>
		  <name><xsl:value-of select="./author/@packager"/></name>
		  <email><xsl:value-of select="./author/@email"/></email>
		</author>
	      </xsl:when>
	       <xsl:when test="./author/@name">
	          <author>
	             <name><xsl:value-of select="./author/@name"/></name>
		     <xsl:if test="./author/@email">
		        <email><xsl:value-of select="./author/@email"/></email>
		     </xsl:if>
	          </author>
	       </xsl:when>
	       <xsl:otherwise>
	          <author>
		     <name>Sylvain Fourmanoit</name>
		     <email>syfou@users.sourceforge.net</email>
		  </author>
	       </xsl:otherwise>
	    </xsl:choose>

	    <xsl:variable name="old_modified_date">
	       <xsl:call-template name="desklet_version_match">
	          <xsl:with-param name="source_title" select="$title" />
	       </xsl:call-template>
            </xsl:variable>
	    <xsl:variable name="modified_date">
	       <xsl:choose>
	          <xsl:when test="$old_modified_date=''">
		     <xsl:value-of select="$now"/>
		  </xsl:when>
		  <xsl:otherwise>
		     <xsl:value-of select="$old_modified_date"/>
		  </xsl:otherwise>
               </xsl:choose>
	    </xsl:variable>

	    <xsl:variable name="old_issued_date">
	       <xsl:call-template name="desklet_name_match">
	          <xsl:with-param name="source_id" select="$id"/>
	       </xsl:call-template>
            </xsl:variable>
	    <xsl:variable name="issued_date">
	       <xsl:choose>
	          <xsl:when test="$old_issued_date=''">
		     <xsl:value-of select="$now"/>
		  </xsl:when>
		  <xsl:otherwise>
		     <xsl:value-of select="$old_issued_date"/>
		  </xsl:otherwise>
               </xsl:choose>
	    </xsl:variable>

	    <issued>
	       <xsl:value-of select="$issued_date" />
	    </issued>
	    <modified>
	       <xsl:value-of select="$modified_date" />
	    </modified>
	    <xsl:element name="generator">
	      <xsl:text>md5:</xsl:text><xsl:value-of select="@md5"/>
	    </xsl:element>
	    <xsl:element name="link">
	       <xsl:attribute name="rel">alternate</xsl:attribute>
	       <xsl:attribute name="type">
	          <xsl:choose>
	             <xsl:when test="@download">application/x-compressed</xsl:when>
	 	  <xsl:otherwise>text/html</xsl:otherwise>
		  </xsl:choose>
	       </xsl:attribute>
	       <xsl:attribute name="href">
	          <xsl:choose>
	             <xsl:when test="@download">
		        <xsl:value-of select="@download"/>
		     </xsl:when>
		     <xsl:otherwise>
		     	<xsl:text>http://prdownloads.sourceforge.net/adesklets/</xsl:text>
		     	<xsl:value-of select="@name" />
			<xsl:text>-</xsl:text>
			<xsl:value-of select="@version" />
			<xsl:text>.tar.bz2?download</xsl:text>
		     </xsl:otherwise>
		  </xsl:choose>
	       </xsl:attribute>
	    </xsl:element>
	    <content mode="xml" type="application/xhtml+xml">
	       <div xmlns="http://www.w3.org/1999/xhtml">
	          <xsl:apply-templates/>
	       </div>
	    </content>
         </entry>
      </xsl:for-each>

      </feed>
   </xsl:template>

<!--
   Returns the existing issued date if given entry existed in
   former version of the atom feed.
!-->
<xsl:template name="desklet_name_match">
   <xsl:for-each select="document('../desklets.atom')//atom:entry">
      <xsl:variable name="id">
         <xsl:value-of select="./atom:id"/>
      </xsl:variable>
      <xsl:if test="$id=$source_id">
         <xsl:value-of select="./atom:issued"/>
      </xsl:if>
   </xsl:for-each>
</xsl:template>

<!--
   Returns the existing modified date if title field has not changed in
   former version of the atom feed.
!-->
<xsl:template name="desklet_version_match">
   <xsl:for-each select="document('../desklets.atom')//atom:entry">
         <xsl:variable name="title">
	    <xsl:value-of select="./atom:title"/>
	 </xsl:variable>
         <xsl:if test="$title=$source_title">
	   <xsl:value-of select="./atom:modified"/>
	 </xsl:if>
   </xsl:for-each>
</xsl:template>

  <!-- Forward port of the HTML equivalent !-->
   <xsl:template match="a">
      <xsl:if test="@href">
      <xsl:element
         name="a">
	 <xsl:attribute name="href">
	    <xsl:value-of select="@href"/>
	 </xsl:attribute>
	 <xsl:apply-templates/>
      </xsl:element>
      </xsl:if>
   </xsl:template>

   <!-- Forward port of the HTML equivalent !-->
   <xsl:template match="p">
      <p><xsl:apply-templates/></p>
   </xsl:template>
       
</xsl:stylesheet>
