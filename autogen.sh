#! /bin/sh
#
# __very__ minimal autogen script

# Helper functions
#
log() {
   echo "+ $*"
   eval $*
   return $?
}

msg() {
   shopt -s xpg_echo > /dev/null 2>&1
   SEP='----------------------------------------------------------------------'
   echo "${SEP}\n$*\n${SEP}"
}

# Now, perform the operation
#
msg "This is just an ordered invokation of the right autotools...\n\
If you do not have what it takes, you are on your own (see the doc)!"

log aclocal && \
	log autoheader && \
	log automake && \
	log autoconf
STATUS=$?
log touch scripting/enums scripting/prototypes

if test ${STATUS} -eq 0 ; then
	msg "That's all folk! See \`./configure --help'.\n\n\
From a freshly extracted code from the development repository, don't\n\
forget to use --enable-maintainer-mode, at least for the first build.\n"
else
	msg "An error occured: autogen.sh failed."
fi
