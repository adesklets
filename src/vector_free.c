/*--- vector_free.c ------------------------------------------------------------
Copyright (C) 2004, 2005, 2006 Sylvain Fourmanoit <syfou@users.sourceforge.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.   
------------------------------------------------------------------------------*/
/* All vector_free_items routines below */

/*----------------------------------------------------------------------------*/
#include "vector_free.h"		

/*----------------------------------------------------------------------------*/
/* vector_free_item_func function for vector of images, fonts, color ranges 
   and color modifiers */

#define VECTOR_FREE_ITEM_FUNCTION(name,NAME)\
void * \
name ## _vector_free_item(void * name)\
{\
  if (name!=NULL) {\
    xwindow_context_save(IMLIB_ ## NAME);\
    imlib_context_set_ ## name(name);\
    imlib_free_ ## name();\
    xwindow_context_restore();\
  }\
 return NULL;\
}

VECTOR_FREE_ITEM_FUNCTION(image,IMAGE)
VECTOR_FREE_ITEM_FUNCTION(font,FONT)
VECTOR_FREE_ITEM_FUNCTION(color_range,COLOR_RANGE)
VECTOR_FREE_ITEM_FUNCTION(color_modifier,COLOR_MODIFIER)
VECTOR_FREE_ITEM_FUNCTION(filter,FILTER)

/*----------------------------------------------------------------------------*/
/* vector_free_item_func for vector of polygons: a bit different because 
   polygon is not part of imlib2's context. */
void *
polygon_vector_free_item(void * polygon)
{
  if(polygon!=NULL)
    imlib_polygon_free(polygon);
  return NULL;
}

/*----------------------------------------------------------------------------*/
