/*--- error.c ----------------------------------------------------------------
Copyright (C) 2004, 2005, 2006 Sylvain Fourmanoit <syfou@users.sourceforge.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.   
------------------------------------------------------------------------------*/
/* Debugging-related functions 
 */

/*----------------------------------------------------------------------------*/
#include "error.h"	   		/* Error header */


/*----------------------------------------------------------------------------*/
int errno_bis;

#ifdef DEBUG
FILE * stdebug = NULL;

/*----------------------------------------------------------------------------*/
/* This initialize debugging correctly: all debug outputs are redirected
 on stderr by default, unless the ADESKLETS_LOG environment variable
 is giving a filename adesklets could open for writing. Called from
 adesklets_init()
*/
void
debug_init(void)
{
  char file[256];
  if (getenv("ADESKLETS_LOG")) {
    snprintf(file,255,"%s.pid%u",getenv("ADESKLETS_LOG"),getpid());
    file[255]=(char)0;
  
    if ((stdebug=fopen(file,"w")))
      setvbuf(stdebug, (char *)NULL, _IOLBF, 0);
  }

  if (!stdebug)
    stdebug=stderr;
}

/*----------------------------------------------------------------------------*/
int
debug(const char * format, ...)
{
  int result;
  va_list ap;
  va_start(ap,format);
  result=vfprintf(stdebug,format,ap);
  va_end(ap);
  return result;
}

/*----------------------------------------------------------------------------*/
int
vdebug(const char * format, va_list ap)
{
  int result;
  va_list apcopy;
  va_copy(apcopy,ap);
  result=vfprintf(stdebug,format,apcopy);
  va_end(apcopy);
  return result;
}

/*----------------------------------------------------------------------------*/
int
debug_on_stderr(void)
{
  return stderr==stdebug;
}

/*----------------------------------------------------------------------------*/
#endif
