#! /bin/sh
#
# 2004, Sylvain Fourmanoit <syfou@users.sourceforge.net>
#
# Shell script that starts an interactive adesklets session
# with the debug (stderr) output in another xterm.
# 

END_MESSAGE='--- End of session ---'
FIFO=/tmp/debug_$$.fifo

# Termination function
#
the_end() {
    rm -f $FIFO
    echo $END_MESSAGE
    read -p 'Kill debug console (Y/N)? ' -n 1 ANSWER
    echo
    echo $ANSWER | grep -i 'y' &>/dev/null && kill $XTERM_PID &>/dev/null
}

# Initial program check
#
export PATH=$PATH:.
for PROG in mkfifo xterm adesklets; do
    test -z `which $PROG 2>/dev/null` && {
	echo Program $PROG could not be found in path. Aborting. 1>&2
	exit 1;
}
done

# Set up exit trapping
#
trap 'the_end' EXIT

# Set up named pipe
#
test ! -p $FIFO && {
    rm -f $FIFO
    mkfifo -m 600 $FIFO
}

# Launch programs
#
xterm -e "cat<$FIFO;echo $END_MESSAGE;echo Press a key to kill console;read" &
XTERM_PID=$!
adesklets : 2>$FIFO
