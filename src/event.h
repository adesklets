/*--- event.h ----------------------------------------------------------------
Copyright (C) 2004, 2005, 2006 Sylvain Fourmanoit <syfou@users.sourceforge.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.   
------------------------------------------------------------------------------*/
/* Event report functions 
 */

/*----------------------------------------------------------------------------*/
#ifndef HAVE_EVENT_H
#define HAVE_EVENT_H

#ifndef HAVE_CONFIG_H
#error Autogenerated config.h should be used.
#endif

/*----------------------------------------------------------------------------*/
#include "config.h"			/* Autoconf header */

#ifdef HAVE_STDIO_H
#include <stdio.h>			/* Standard IO routines  */
#endif

#ifdef HAVE_STRING_H			/* strcpy() */
#include <string.h>
#endif

#ifdef HAVE_STDARG_H			/* Variadic functions */
#include <stdarg.h>
#endif

#ifdef HAVE_SYS_TYPES_H			/* pid_t typedef */
#include <sys/types.h>
#endif

#ifdef HAVE_SIGNAL_H			/* kill() */
#include <signal.h>
#endif

#include "types.h"			/* Various typedef */
#include "error.h"			/* Error wrapper */
#include "vector.h"                     /* Small vector API */

/*----------------------------------------------------------------------------*/
extern pid_t    events_ppid;
extern int      events_echo;
extern int      events_delay;
extern vector * events;

/*----------------------------------------------------------------------------*/
/* Event output formatting function */
void event(char *, ...);

/* Event cache purge */
void events_purge(void);

/*----------------------------------------------------------------------------*/
#endif
