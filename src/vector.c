/*--- vector.c -----------------------------------------------------------------
Copyright (C) 2004, 2005, 2006 Sylvain Fourmanoit <syfou@users.sourceforge.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.   
------------------------------------------------------------------------------*/
/* This implements a minimalistic vector API with error checking,        
   useful for storing dynamic collections of structures. 
   
   Have a look at vector.h for a more detailed description. */

/*----------------------------------------------------------------------------*/
#include "vector.h"		/* Vector header */

/*----------------------------------------------------------------------------*/
void *
default_free(void* item)
{
  TRY_RESET; TRY(free(item));
  return (TRY_SUCCESS)?NULL:item;
}

/*----------------------------------------------------------------------------*/
int
vector_resize(vector* vec, uint newsize) {
  uint i;
  if (!vec) return 0L;
  if((newsize>vec->size)||((newsize<vec->size)&&(newsize>vec->pos))) {
    if(vec->size==0)
      TRY_CATCH(vec->content=malloc(sizeof(void*)*newsize));
    else
      TRY_CATCH(vec->content=realloc(vec->content,sizeof(void*)*newsize));
    if(newsize>vec->size)
      for(i=vec->size;i<newsize;++i) vec->content[i]=NULL;
    vec->size=newsize;
  }
  return 1;
}

/*----------------------------------------------------------------------------*/
vector *
vector_init(void) 
{
  vector * vec;
  TRY_CATCH(vec = (vector *)malloc(sizeof(vector)));
  vec->pos=vec->size=0;
  vec->vector_free_item=default_free;
  if (!vector_resize(vec,VECTOR_CHUNK)) {
    free(vec);
    vec=NULL;
  }
  return vec;
}

/*----------------------------------------------------------------------------*/
int
vector_push(vector * vec, void * elem)
{
  int result = 1;
  if (vec!=NULL && elem!=NULL) {
    if(vec->pos==vec->size)
      result=vector_resize(vec,vec->size+VECTOR_CHUNK);
    if (result) vec->content[vec->pos++]=elem;
  } else result=0;
  return result;
}

/*----------------------------------------------------------------------------*/
int
vector_pop(vector * vec) 
{
  int result = 1;
  if(!vec) return 0L;
  if (vec->pos>0) {
    --vec->pos;
    if(vec->pos%VECTOR_CHUNK==VECTOR_CHUNK-1 && vec->pos!=0)
      result=vector_resize(vec,vec->pos+1);
    if (result)
      if(vec->content[vec->pos])
	result=(vec->content[vec->pos]=
	  vec->vector_free_item(vec->content[vec->pos]))==NULL;
  } else result=0;
  return result;
}

/*----------------------------------------------------------------------------*/
int
vector_insert(vector * vec, uint pos, void * elem) 
{
  uint i;
  int result = 1;
  if (!vec) return 0L;
  if (pos==vec->pos)
    result=vector_push(vec,elem);
  else
    if (pos<vec->pos) {
      if(vec->pos==vec->size)
	result=vector_resize(vec,vec->size+VECTOR_CHUNK);
      if (result) {
	if(vec->pos)
	  for(i=vec->pos;i>pos;--i)
	    vec->content[i]=vec->content[i-1];
	vec->content[pos]=elem;
	++vec->pos;
      }
    } else result=0;
  return result;
}

/*----------------------------------------------------------------------------*/
int 
vector_delete(vector* vec, uint pos)
{
  uint i;
  int result = 1;
  if (!vec) return 0L;
  if(pos<vec->pos) {
    if (!(vec->content[pos]=vec->vector_free_item(vec->content[pos]))) {
      for(i=pos;i<vec->pos-1;++i)
	vec->content[i]=vec->content[i+1];
      --vec->pos;
      if(vec->pos%VECTOR_CHUNK==VECTOR_CHUNK-1 && vec->pos!=0)
	result=vector_resize(vec,vec->pos+1);
    } else result=0;
  } else result=0;
  return result;
}

/*----------------------------------------------------------------------------*/
void * 
vector_find(vector * vec , uint * i, 
	    vector_search_func search_func, void *callback)
{
  if (!vec || !i) return 0L;
  for(*i=0;*i<vec->pos;++*i)
    if (search_func(vec->content[*i],callback))
      return vec->content[*i];
  return NULL;
}

/*----------------------------------------------------------------------------*/
vector *
vector_free(vector * vec)
{
  if (!vec) return NULL;
  while(vector_pop(vec));  /* This is certainly not the quickest way
			      to empty a vector, but it is a simple
			      method to ensure its coherence when it fails. 
			    */
  if (!vec->pos) {
    free(vec->content);
    free(vec);
    return NULL;
  } else return vec;
}
