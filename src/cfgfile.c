/*--- cfgfile.c ----------------------------------------------------------------
Copyright (C) 2004, 2005, 2006 Sylvain Fourmanoit <syfou@users.sourceforge.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.   
------------------------------------------------------------------------------*/
/* Management routines for adesklets configuration file 
 */

/*----------------------------------------------------------------------------*/
#include "cfgfile.h"		/* Adesklets configuration file header */ 

/*----------------------------------------------------------------------------*/
/* Useful prototypes from the flex scanner and bison parser
 */
int cfgparse_init(vector *);
int cfgparse(void);

extern FILE * cfgin;

/*----------------------------------------------------------------------------*/
/* Return canonical filename of configuration file, 
   NULL if it couldn't be retrieved.
 */
char * const
cfgfile_name(void) {
  static char filename[CFGFILE_NAME_SIZE] = "";
  char * home;
  struct passwd * info;

  if ((home = getenv("HOME"))) {
    TRY_CATCH(strncpy(filename,home,CFGFILE_NAME_SIZE-1));
  } else {
    TRY_CATCH(info = getpwuid(getuid()));
    if (info->pw_dir==NULL) return 0L;
    TRY_CATCH(strncpy(filename,info->pw_dir,CFGFILE_NAME_SIZE-1));
  }
  TRY_CATCH(strncat(filename,\
		    CFGFILE_NAME,\
		    CFGFILE_NAME_SIZE-strlen(filename)-1));
  debug("Configuration file name: `%s'\n",filename);
  return filename;
}

/*----------------------------------------------------------------------------*/
/* Initialize and call the configuration file parser.
   Returns a vector of cfgfile_item structures if successful,
   NULL otherwise.
 */
vector *
cfgfile_parse(FILE * fd) 
{
#ifdef DEBUG
  int i;
#endif
  vector * vec;
  
  TRY_CATCH(fseek(fd,0L,SEEK_SET));

  if (cfgparse_init(vec=vector_init())) {
    cfgin=fd;
    if (cfgparse()==1) {
      debug("Error parsing configuration file\n");
      vec=vector_free(vec);
    }
#ifdef DEBUG
    else {
      debug("Configuration file parsing results:\n");
      for (i=0;i<vec->pos;++i)
	debug("[%s] id=%u screen=%d x=%u y=%u\n",
	       ((cfgfile_item*)vec->content[i])->applet,
	       ((cfgfile_item*)vec->content[i])->id,
	       ((cfgfile_item*)vec->content[i])->scr,
	       ((cfgfile_item*)vec->content[i])->x,
	       ((cfgfile_item*)vec->content[i])->y);
    }
#endif
  }
  return vec;
}

/*----------------------------------------------------------------------------*/
/* Open adesklets' configuration file, using a POSIX advisory locking 
   mechanism. Returns reference to FILE if successful, NULL otherwise.
   Try creating the file if it do not exists.

   WARNING: this routine will block until all other locks on the file
            are removed by concurrent process. Therefore, operations
	    between successive calls to cfgfile_open() and cfgfile_close()
	    should stay as short as possible!
*/
FILE * 
cfgfile_open()
{
  char * filename;
  FILE * fd;
  struct flock lock;

  fd = NULL;
  if ((filename=cfgfile_name())) 
    if ((fd=fopen(filename,"r+"))||
	(fd=fopen(filename,"w+"))) {
      lock.l_type=F_WRLCK;
      lock.l_whence=SEEK_SET;
      lock.l_start=0;
      lock.l_len=0;
      TRY_CATCH(fcntl(fileno(fd),F_SETLKW,&lock));
    }
  return fd;
}

/*----------------------------------------------------------------------------*/
/* Close configuration file fd and remove fcntl advisory lock.
   Returns 1 on success, 0 on failure.
 */
int 
cfgfile_close(FILE * fd) 
{
  struct flock lock;
  
  lock.l_type=F_UNLCK;
  lock.l_whence=SEEK_SET;
  lock.l_start=0;
  lock.l_len=0;
  TRY_CATCH(fcntl(fileno(fd),F_SETLK,&lock));
  TRY_CATCH(fclose(fd));
  return 1L;
}

/*----------------------------------------------------------------------------*/
struct s_cfg_search_applet {
  char * applet;
  uint id;
};

/*----------------------------------------------------------------------------*/
int 
cfg_search_applet(void * elem, void* callback) 
{
  return (strncmp(((cfgfile_item*)elem)->applet,
		 ((struct s_cfg_search_applet*)callback)->applet,
		 256) == 0) && 
         ((cfgfile_item*)elem)->id ==
         ((struct s_cfg_search_applet*)callback)->id;
}

/*----------------------------------------------------------------------------*/
int
cfg_search_highest_applet(void * elem, void * callback)
{
  if (strncmp(((cfgfile_item*)elem)->applet,
	      ((struct s_cfg_search_applet*)callback)->applet,
	      256) == 0 &&
      (((cfgfile_item*)elem)->id > ((struct s_cfg_search_applet*)callback)->id ||
       ((struct s_cfg_search_applet*)callback)->id==(uint)-1))
    ((struct s_cfg_search_applet*)callback)->id=((cfgfile_item*)elem)->id;
  return 0;
}

/*----------------------------------------------------------------------------*/
/* Get an applet caracteristics from configuration file.
   Always return the reference, dutyfully filled with zeroes
   if not describded. If given id is ((uint)-1), attribute next
   free id for applet

   Note : This code is not made to be used in reentrant context,
          so feel free to modify returned content.
 */
cfgfile_item * 
cfgfile_getinfo(char * applet, uint id)
{
  uint i;
  char str[2] = "-";
  FILE * fd;
  vector * vec;
  static cfgfile_item item; 
  cfgfile_item * lpitem = NULL;
  struct s_cfg_search_applet cfg_search;

  
  if ((fd=cfgfile_open())) {
    if ((vec=cfgfile_parse(fd))) {
      cfgfile_close(fd);
      
      cfg_search.applet=(applet)?applet:(char*)&str;
      if (id!=(uint)-1) {
	cfg_search.id=id;
	if ((lpitem = vector_find(vec,&i,cfg_search_applet,&cfg_search))) {
	  strncpy(item.applet,lpitem->applet,CFGFILE_NAME_SIZE-1);
	  item.applet[CFGFILE_NAME_SIZE-1]=0;
	  item.id=lpitem->id;
	  item.scr=lpitem->scr;
	  item.x=lpitem->x;
	  item.y=lpitem->y;
	  item.no_update=lpitem->no_update;
	}
      } else {
	if (applet) {
	  cfg_search.id=(uint)-1;
	  vector_find(vec,&i,cfg_search_highest_applet,&cfg_search);
	  id=(uint)(((int)cfg_search.id)+1);
	  debug("New id: %u\n",id);
	} else id=0;
      }

      vec=vector_free(vec);
    } else cfgfile_close(fd);
  }

  if (!lpitem) {
    strncpy(item.applet,(applet)?applet:(char*)&str,CFGFILE_NAME_SIZE-1);
    item.applet[CFGFILE_NAME_SIZE-1]=0;
    item.id=id;
    item.x=item.y=item.no_update=0;
    item.scr=-1;                            /* Invalid screen reference:
					       force update in adesklets_init()
					    */
  }

  debug("Configuration file info: [%s] id=%u screen=%d x=%d y=%d\n",
	item.applet, item.id, item.scr, item.x, item.y);

  return &item;
}

/*----------------------------------------------------------------------------*/
/* Update an applet description in configuration file, remove it
   if no_update flags sets to 1.
   Return 1 on success, 0 on failure.
*/
int
cfgfile_update(cfgfile_item * new_item) 
{
  int is_new = 0, result = 1;
  uint i;
  FILE * fd;
  vector * vec;
  cfgfile_item * item;
  struct s_cfg_search_applet cfg_search;
  
  const char * cfgfile_message[] = {
    "# This is adesklets configuration file.",
    "#",
    "# It gets automatically updated every time a desklet main window",
    "# parameter is changed, so avoid manual modification whenever",
    "# a desklet is running. In fact, manual changes to this file",
    "# should rarely be needed. See `info adesklets'.\n"
  };

  TRY_RESET;
  if (new_item->applet) {
    /* Open the file */
    if ((fd=cfgfile_open())) {
      /* Parse the file */
      if ((vec=cfgfile_parse(fd))) {
	/* Find item, if it exists */
	cfg_search.applet=new_item->applet;
	cfg_search.id=new_item->id;
	if (!(item = vector_find(vec,&i,cfg_search_applet,&cfg_search)))
	  TRY(item=malloc(sizeof(cfgfile_item));is_new=1);
	/* And delete it, if asked for */ 
	else if (new_item->no_update) {
	  debug("Configuration file update: removing an entry\n");
	  result=vector_delete(vec,i);
	}
	/* Copy updated data */
	if (!new_item->no_update && TRY_SUCCESS) {
	  strncpy(item->applet,new_item->applet,256);
	  item->id=new_item->id;
	  item->scr=new_item->scr;
	  item->x=new_item->x;
	  item->y=new_item->y;
	  if(is_new) result=vector_push(vec,item);
	} else {
	  if (is_new) free(item);
	  if (!TRY_SUCCESS) result=0L;
	}
	/* Write everything back to configuration file */
	if (result) {
	  debug("Configuration file: write everything back\n");
	  TRY(fseek(fd,0L,SEEK_SET));
	  if (TRY_SUCCESS) {
	    for (i=0;i<6;++i) fprintf(fd,"%s\n",cfgfile_message[i]);
	    for(i=0;i<vec->pos;++i)
	      fprintf(fd,"[%s]\nid=%u screen=%d x=%u y=%u\n\n",
		      ((cfgfile_item*)vec->content[i])->applet,
		      ((cfgfile_item*)vec->content[i])->id,
		      ((cfgfile_item*)vec->content[i])->scr,
		      ((cfgfile_item*)vec->content[i])->x,
		      ((cfgfile_item*)vec->content[i])->y);
	    if (ftruncate(fileno(fd),ftell(fd))==-1) result=0L;
	  } else result=0L;
	}
	vector_free(vec);
      } else result=0L;
      cfgfile_close(fd);
    } else result=0L;
  } else result=0L;
  return result;
}

/*----------------------------------------------------------------------------*/
/* Execute every desklet described in the configuration file, 
   setting the ADESKLETS_ID environment variable as needed.
   Returns 1 on success, 0 on failure.
*/
int
cfgfile_loadall(void)
{
  char * filename;
  int tmp_result = 0, result = 1, fd_null, pid;
  uint i;
  char *dot, * dpy_in, *dpy_out,
    id_str[10]; /* See cfg_scanner.l for size justification */
  FILE * fd;
  struct dirent * entry;
  DIR * dir;
  struct flock lock;
  vector * vec;
  
  /* Clean up all lock files */
  if ((dir=opendir(LOCKFILES_DIR))) {
    for(;(entry=readdir(dir));)
      if (strstr(entry->d_name,"adesklets")==entry->d_name) {
	filename=malloc(sizeof(char)*(strlen(entry->d_name)+
				      strlen(LOCKFILES_DIR)+2));
	strcpy(filename,LOCKFILES_DIR);
	strcat(filename,"/");
	strcat(filename,entry->d_name);
	if ((fd=fopen(filename,"r+"))) {
	  lock.l_type=F_WRLCK;
	  lock.l_whence=SEEK_SET;
	  lock.l_start=0;
	  lock.l_len=0;
	  fcntl(fileno(fd),F_GETLK,&lock);
	  if (lock.l_type!=F_UNLCK) {
	    /* Action has to be taken to terminate older, 
	       still running desklets */
	    debug("desklets process associated with lockfile '%s' %s",
		  filename, "are still runnning.\n");
	    while(fscanf(fd,"%d",&pid)==1)
	      kill(pid,SIGKILL);
	  }
	  unlink(filename);
	  debug("Unlinking '%s'\n",filename);
	  fclose(fd);
	}
	free(filename);
      }
    closedir(dir);
  }
  
  if ((fd=cfgfile_open()))
    if ((vec=cfgfile_parse(fd))) {
      cfgfile_close(fd);
      /* Here we go: let's fork and execute */
      for(i=0;i<vec->pos;++i) 
	if((result = result && ((tmp_result = fork())>=0)), !tmp_result ) {
	  /* Charge the correct ID into the environment */
	  if(snprintf(id_str,9,"%u",((cfgfile_item*)vec->content[i])->id)>=1)
	    setenv("ADESKLETS_ID",id_str,1);
	  /* Then build up a correct DISPLAY variable (i.e. with proper default 
	     screen). Of course, this is redundant, since the adesklets 
	     interpreter doesn't need this information; but the wrapper script
	     could, since it may want to find a fake root window ID. Look 
	     at the XOpenDisplay() documentation for the complete description 
	     of the DISPLAY variable.
	  */
	  if ((dpy_in=getenv("DISPLAY")) &&
	      (dot=strrchr(dpy_in,'.')) &&
	      (dpy_out=malloc(strlen(dpy_in)+10))) {
	    strcpy(dpy_out,dpy_in);
	    if (snprintf(dpy_out+(dot-dpy_in)+1,9,"%u",
			 ((cfgfile_item*)vec->content[i])->scr)>=0)
	      setenv("DISPLAY", dpy_out, 1);
	    free(dpy_out);
	  }

	  /* Redirect stdout and stderr to /dev/null 
	     to avoid possible output overflow */
	  if ((fd_null=open("/dev/null",O_WRONLY))>=0) {
	    dup2(fd_null,1);
	    dup2(fd_null,2);
	    close(fd_null);
	  }

	  /* Try changing to the base directory of the desklet 
	   */
	  if (!((filename = 
		 strdup((((cfgfile_item*)vec->content[i])->applet))) &&
		(chdir(dirname(filename)) == 0)))
	    debug("Could not change to base directory of `%s'",
		  (((cfgfile_item*)vec->content[i])->applet));
	  free(filename);

	  execl(((cfgfile_item*)vec->content[i])->applet,
		((cfgfile_item*)vec->content[i])->applet,
		NULL);
	  /* debug("Could not exec: `%s'\n",
	     ((cfgfile_item*)vec->content[i])->applet); */
	  exit(EXIT_FAILURE);
	}
      vector_free(vec);
    } else {
      cfgfile_close(fd);
      result=0L;
    }
  else result=0L;

  return result;
}
