/*--- variable.c -------------------------------------------------------------
Copyright (C) 2004, 2005, 2006 Sylvain Fourmanoit <syfou@users.sourceforge.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.   
------------------------------------------------------------------------------*/
/* Textual variables related functions 
 */

/*----------------------------------------------------------------------------*/
#include "variable.h"	   		/* Variable header */

/*----------------------------------------------------------------------------*/
extern char * dupstr(const char *s); 	/* Defined in command.c */

/*----------------------------------------------------------------------------*/
var_item *
variable_create(const char * name, const char * value)
{
  var_item * item;
  if ((item=malloc(sizeof(var_item)))) {
    item->name=dupstr(name);
    item->value=dupstr(value);
  }
  return item;
}

/*----------------------------------------------------------------------------*/
int
variable_search_func(void* item, void* name)
{
  return strcmp(((var_item*)item)->name,(char*)name)==0;
}

/*----------------------------------------------------------------------------*/
/* This is the big ugly variable expansion routine. It takes each first group
   from matching /$([^$[:space:]]*)/ and expands it to its found value 
   in the *vars vertor. C is definitely not the best tool 
   for string manipulation.
 */
void
variable_expansion(vector * vars, char ** old)
{
  int i,j,k,l,size;
  uint dummy;
  char * new, * var;
  var_item * v_item;
  
  if(*old) {
    size=strlen(*old)+1;
    if ((new=malloc(sizeof(char)*size))) {
      for(i=0,j=0;(*old)[i];++i) {
	if ((*old)[i]!='$')
	  new[j++]=(*old)[i];
	else
	  if ((*old)[i+1]!='$') {
	    for(k=i+1;
		(*old)[k] && 
		  (*old)[k]!=' ' && (*old)[k]!='\t' && (*old)[k]!='$';
		++k);
	    if((var=malloc(sizeof(char)*(k-i)))) {
	      strncpy(var,(*old)+i+1,(k-i-1));
	      var[k-i-1]=(char)0;
	      if ((v_item=vector_find(vars,
				      &dummy,
				      variable_search_func,var))) {
		size+=strlen(v_item->value)+1;
		new=realloc(new,sizeof(char)*size);
		for(l=0;v_item->value[l];++l)
		  new[j+l]=v_item->value[l];
		j+=l;
	      }
	      free(var);var=NULL;
	    }
	    i=k-1;
	  } else {
	    new[j++]='$';
	    if ((*old)[i+1])
	      ++i;
	  }
      }
      new[j]=0;
      free(*old);
      (*old)=new;
    }
  }
}

/*----------------------------------------------------------------------------*/
void * 
variable_vector_free_item(void* item)
{
  TRY_RESET; 
  TRY(free(((var_item*)item)->name));
  if (TRY_SUCCESS) {
    TRY(free(((var_item*)item)->value));
    if (TRY_SUCCESS)
      TRY(free(item));
  }
#ifdef DEBUG
  if (!TRY_SUCCESS)
    debug("Ouch! Memory leak while freeing variable.");
#endif
  return NULL;
}

/*----------------------------------------------------------------------------*/
