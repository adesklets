/*--- cfg_scanner.l ------------------------------------------------------------
Copyright (C) 2004, 2005, 2006 Sylvain Fourmanoit <syfou@users.sourceforge.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.   
------------------------------------------------------------------------------*/
/* Configuration file lexer (for flex).
*/

/*----------------------------------------------------------------------------*/
/* Option definition to bypass my automake installation limitation.
   WARNING: remove the never-interactive flag for debugging! 
*/
%option noyywrap never-interactive case-insensitive prefix="cfg" outfile="lex.yy.c"

/*----------------------------------------------------------------------------*/
%{
#ifdef HAVE_CONFIG_H		/* Autoconf header */
#include "config.h"
#endif

#ifdef HAVE_STRING_H		/* Strncpy function */
#include <string.h>
#endif

#include "cfg_parser.h"		/* Bison Generated Tokens */
  
int cfg_lineno=1;
%}
/*----------------------------------------------------------------------------*/
%%
\n 				{ ++cfg_lineno; }
[0-9]{1,9} 			{ /* In fact, there is both integer and
                                     unsigned integer used... But since
                                     there is no use declaring screen
                                     of more than 2^31 pixels witdh,
                                     we just limit ourselves to 9 digits,
                                     hence slightly below the positive
                                     integer mark. */
                                  cfglval.cfg_int=atoi(cfgtext); 
				                            return INTEGER; }
\[\/[^\]]{1,254}\]               { strncpy(cfglval.cfg_char,
					  cfgtext+1,cfgleng-2);
				   cfglval.cfg_char[cfgleng-2]=0;    
                                                            return APPLET;  }
[[:space:]]*id[[:space:]]*=     {                           return ID;      }
[[:space:]]*screen[[:space:]]*= {                           return SCREEN;  }
[[:space:]]*x[[:space:]]*=      {                           return X;       }
[[:space:]]*y[[:space:]]*=      {                           return Y;       }

#.*
[[:space:]]			{ /* Comments and space: do nothing */      }
.                               {                           return UNKNOWN; }
%%
/*----------------------------------------------------------------------------*/
