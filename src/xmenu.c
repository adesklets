/*--- xmenu.c ------------------------------------------------------------------
Copyright (C) 2004, 2005, 2006 Sylvain Fourmanoit <syfou@users.sourceforge.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.   
------------------------------------------------------------------------------*/
/* Simple X Windows menu API */

/*----------------------------------------------------------------------------*/
#include "xmenu.h"			/* xmenu headers */

#ifdef HAVE_STRING_H
#include <string.h>			/* strlen, strcpy functions */
#endif

/*----------------------------------------------------------------------------*/
#define XMENU_X_BORDER 10
#define XMENU_Y_BORDER 5

/*----------------------------------------------------------------------------*/
/* This is logically part of command.h/command.c, but we need it here also.
 */
extern char * dupstr_utf8(const char *);

/*----------------------------------------------------------------------------*/
xmenu * 
xmenu_init(void) 
{
  xmenu * menu;
  if ((menu=(xmenu*)malloc(sizeof(xmenu)))) {
    if (!(menu->content=tree_init())) {
      free(menu);
      menu=NULL;
    } else 
      menu->pos=menu->content;
  }
  return menu;
}

/*----------------------------------------------------------------------------*/
int
xmenu_push_item(xmenu * menu, const char * item)
{
  int result=0;
  char * str;
  if(menu && item) {
    TRY_CATCH(str=malloc(strlen(item)+1));
    strcpy(str,item);
    if (tree_push(menu->pos,str))
      result=1;
  }
  return result;
}

/*----------------------------------------------------------------------------*/
int
xmenu_push_submenu(xmenu * menu, const char * item)
{
  int result=0;
  char * str;

  if(menu && item) { 
      TRY_CATCH(str=malloc(strlen(item)));
      strcpy(str,item);
      if (tree_push(menu->pos,str)) {
	menu->pos=
	  (tree*)menu->pos->children->content[menu->pos->children->pos-1];
	result=1;
      }
  }
  return result;
}

/*----------------------------------------------------------------------------*/
int
xmenu_end_submenu(xmenu * menu)
{
  return (menu->pos->parent)?(menu->pos=menu->pos->parent)!=NULL:0;
}

/*----------------------------------------------------------------------------*/
void 
xmenu_list_callback(tree* mytree,uint depth, uint pos, void *params) {
  printf("String: %s\tDepth: %u\tPos: %u\n",(char*)mytree->content,depth,pos);
}

/*----------------------------------------------------------------------------*/
int
xmenu_build_image(tree *pos, int width, int height, int active)
{
  int i,h,x,y,
    result=0;
  char * item;
  Imlib_Image image;
  Imlib_Font font;
  ImlibPolygon poly;

  if ((font=imlib_load_font("Vera/12")) &&
      (image=imlib_create_image(width,height))) {
    imlib_context_set_font(font);
    imlib_context_set_image(image);
    /* Fill image in light grey */
    imlib_context_set_color(200, 200, 200, 255);
    imlib_image_fill_rectangle(0,0,width,height);
    imlib_context_set_color(0,0,0,255);
    /* Iterate and generate text, normal or inverted */
    for(h=XMENU_Y_BORDER,i=0;i<pos->children->pos;++i,h+=y) {
      item=dupstr_utf8(((tree*)(pos->children->content[i]))->content);
      if (*((char*)((tree*)pos->children->content[i])->content)!='-')
	if (item)
	  imlib_get_text_size(item, &x, &y);
	else
	  x=y=0;
      else {
	x=-1; y=XMENU_Y_BORDER;
      }

      if(i==active) {
	imlib_image_fill_rectangle(0,h,width,y);
	imlib_context_set_color(255,255,255,255);
      }
      if (x!=-1) {
	if (item) imlib_text_draw(XMENU_X_BORDER,h,item);
      } else
	imlib_image_draw_line(0,h+y/2,width,h+y/2,0);

      /* Potential submenu item: show it! */
      if (((tree*)(pos->children->content[i]))->children->pos) {
	if ((poly=imlib_polygon_new())) {
	  imlib_polygon_add_point(poly,width-XMENU_X_BORDER+3,h+y);
	  imlib_polygon_add_point(poly,width-XMENU_X_BORDER+3,h);
	  imlib_polygon_add_point(poly,width,h+y/2);
	  imlib_image_fill_polygon(poly);
	  imlib_polygon_free(poly);
	}
	if ((poly=imlib_polygon_new())) {
	  imlib_polygon_add_point(poly,XMENU_X_BORDER-3,h+y);
	  imlib_polygon_add_point(poly,XMENU_X_BORDER-3,h);
	  imlib_polygon_add_point(poly,0,h+y/2);
	  imlib_image_fill_polygon(poly);
	  imlib_polygon_free(poly);
	}
      }
      if (i==active) imlib_context_set_color(0,0,0,255);
      free(item);
    }
    imlib_free_font();
    result=1;
  }
  return result;
}

/*----------------------------------------------------------------------------*/
/* This (too long) function just generates a menu, prints it, and manages it
   until the user made its choice, including submenu generation and
   parent window update. Returns a pointer to the string of what was selected,
   or null otherwise.
*/
char * 
xmenu_fire(Display * display, int scr, 
	   Window parent, 
	   Imlib_Image parent_background, Imlib_Image parent_foreground,
	   int transparency,
	   xmenu * menu)
{
#ifndef X_DISPLAY_MISSING
  char *item, * result = NULL;
  int x, y, z, width, height, depth, 
    active_item, exit_flag;
  int * heights;
  tree * pos;
  Window window;
  Visual * visual;
  XWindowAttributes attr;
  XEvent ev;
  Imlib_Updates updates;
  Imlib_Font font;

  if (menu) {
    /* Set original position */
    pos=menu->content;
        
    /* Return silently if no items */
    if(!pos->children->pos) return NULL;

    do {
      active_item=-1; exit_flag=0;

    TRY_CATCH(heights=(int*)malloc(sizeof(int)*pos->children->pos));
    
    /* Imlib2 context save */
    xwindow_context_save(IMLIB_VISUAL|IMLIB_DRAWABLE|
			 IMLIB_IMAGE|IMLIB_FONT|IMLIB_DIRECTION);
    /* Determine width and height of menu */
    imlib_context_set_direction(IMLIB_TEXT_TO_RIGHT);
    if ((font=imlib_load_font("Vera/12"))) {
      /* Get screen dimentions via its root window attributes */
      XGetWindowAttributes(display,
			   RootWindowOfScreen(ScreenOfDisplay(display,scr)),
			   &attr);
      imlib_context_set_font(font);
      for(width=0,height=0,z=0;
	  z<pos->children->pos;
	  heights[z++]=(height+=y),width=(x>width)?x:width) {
	if (*((char*)((tree*)pos->children->content[z])->content)!='-') {
	  /* Real text */
	  if ((item=dupstr_utf8(((tree*)(pos->children->content[z]))->content))) {
	    imlib_get_text_size(item, &x, &y);
	    free(item);
	  } else 
	    x=y=0;
	} else {
	  /* Separator */
	  x=0; y=XMENU_Y_BORDER;
	}
      }
      imlib_free_font();

      /* Verify that there is something to write */ 
      if (width && height) {
	width+=2*XMENU_X_BORDER; height+=2*XMENU_Y_BORDER;
	/* Get actual pointer position: to work, the pointer need
	   to be on the same screen */
	if(XQueryPointer(display,
			 RootWindowOfScreen(ScreenOfDisplay(display,scr)),
			 &window,&window,&x,&y,&z,&z,(uint*)&z)) {
	  /* Windows coordinates computation */
	  x=((x-=width/2)<0)?0:x;
	  y=((y-=height/2)<0)?0:y;
	  x=((x+width)<=attr.width)?x:attr.width-width;
	  y=((y+height)<=attr.height)?y:attr.height-height;
	  /* Create the window */
	  if((window=
	      XCreateWindow(display,
			    RootWindowOfScreen(ScreenOfDisplay(display,scr)),
			    x, y, width, height, 0,
			    DefaultDepth(display,scr),
			    InputOutput,
			    visual=
			    imlib_get_best_visual(display,scr,&depth),
			    CWOverrideRedirect|CWBackPixel,
			    &XDefaultWindowAttributes))) {
	    /* Select events */
	    XSelectInput(display, window, 
			 PointerMotionMask | ButtonPressMask | 
			 ExposureMask | LeaveWindowMask);
	    /* Imlib2 settings */
	    imlib_context_set_visual(visual);
	    imlib_context_set_drawable(window);
	    
	    /* Finally, map and manage the window */
	    XMapRaised(display,window);
	    do {
	      updates = imlib_updates_init();
	      while(XCheckWindowEvent(display,parent,
				      ExposureMask,
				      &ev) ||
		    XCheckWindowEvent(display,window,
				      PointerMotionMask | ButtonPressMask | 
				      ExposureMask | LeaveWindowMask,
				      &ev)) {
		switch(ev.type) {
		case Expose:
		  if(ev.xexpose.window==parent) {
		    /* Refresh updates list */
		    updates = imlib_update_append_rect(updates,
						       ev.xexpose.x,
						       ev.xexpose.y,
						       ev.xexpose.width,
						       ev.xexpose.height);
		    break;
		  }
		case MotionNotify:
		  for(z=0; 
		      z<pos->children->pos && 
			((XMotionEvent*)&ev.xbutton)->y-XMENU_Y_BORDER>
			heights[z];
		      ++z);
		  z=(z==pos->children->pos)?pos->children->pos-1:z;
		  z=(*((char*)((tree*)pos->children->content[z])->content)
		     !='-')?z:active_item;
		  if(z!=active_item && 
		     xmenu_build_image(pos,width,height,z)) {
		    imlib_render_image_on_drawable(0,0);
		    imlib_free_image();
		    active_item=z;
		  }
		  break;
		case LeaveNotify:
		  active_item=-1;
		case ButtonPress:
		  exit_flag=1;
		  break;
		}
	      }

	       /* Perform parent window updates as needed */
	      xwindow_update_window(parent, &updates,
				    parent_background,
				    parent_foreground,
				    transparency);
	      if(updates) imlib_updates_free(updates);

	      /* Now a bit of sleep */
	      usleep(X_POLLING_PERIOD);
	    } while(!exit_flag);

	    /* Unmap and destroy the window */
	    XUnmapWindow(display,window);
	    XDestroyWindow(display,window);
	    /* !!! */
	    XSync(display, False);

	    /* Others cleaning */
	    free(heights);
	  }
	}
      }
    }
    /* Imlib2 restore original context settings */
    xwindow_context_restore();
    /* Do it again entirely if the user clicked on a submenu */
    } while(active_item!=-1 && 
	    (((tree*)pos->children->content[active_item])->children->pos)?
	    pos=(tree*)pos->children->content[active_item],1:0);
		     
    result=(active_item!=-1)?
      ((char*)((tree*)pos->children->content[active_item])->content):NULL;
  }
  return result;
#else /* X_MISSING_DISPLAY */
  return NULL;
#endif
}

/*----------------------------------------------------------------------------*/
xmenu * 
xmenu_free(xmenu * menu)
{
  if ((menu->content=tree_free(menu->content))==NULL) {
    free(menu);
    menu=NULL;
  }
  return menu;
}

/*----------------------------------------------------------------------------*/
void * 
xmenu_vector_free_item(void* menu)
{
  return xmenu_free((xmenu*)menu);
}

/*----------------------------------------------------------------------------*/
