/*--- cfg_parser.y -------------------------------------------------------------
Copyright (C) 2004, 2005, 2006 Sylvain Fourmanoit <syfou@users.sourceforge.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.   
------------------------------------------------------------------------------*/
/* Configuration file parser (bison)                        
*/
 
/*----------------------------------------------------------------------------*/

%{
  #include "cfgfile.h"					/* Configuration file
							   routines */
  
  int cfglex(void);                                     /* Scanner exports */
  extern int cfg_lineno;                                    
  
  void cfgerror(char * str)				/* Error message */ 
  {
    fprintf(stderr,
	    "Configuration file parsing error `%s', stopped at line %d.\n",
	    str, cfg_lineno);
  }

  int cfgparse_alloc(void);				/* Space allocation */
  cfgfile_item * cfg_item;				/* Item reference 
							   to be pushed back 
                                                           on vector */
  vector * cfg_vec;					/* Callback vector */

  #define CFG_ITEM_ALLOC \
  if(!cfg_item) if (!cfgparse_alloc()) {\
	cfgerror("memory allocation problem"); \
	YYABORT; \
  }

%}

/*----------------------------------------------------------------------------*/
%union {
  int cfg_int;
  char cfg_char[256];
}
  
%token <cfg_int> INTEGER
%token <cfg_char> APPLET 
%token ID SCREEN X Y UNKNOWN

/*----------------------------------------------------------------------------*/
%%

cfgfile: /* empty */ |
	 cfgfile item
;

item:  APPLET body
	{
	  strncpy(cfg_item->applet,$1,CFGFILE_NAME_SIZE-1);
	  if (!vector_push(cfg_vec,cfg_item))
	    YYABORT;
	  cfg_item=NULL;
	}
| UNKNOWN { cfgerror("unknown token encountered"); YYABORT; }
;

body: body_elem |
	body body_elem
;

body_elem:
	id_elem |
	screen_elem |
	x_elem |
	y_elem
;

id_elem: ID INTEGER
	{CFG_ITEM_ALLOC; cfg_item->id=$2;}

screen_elem: SCREEN INTEGER
	{CFG_ITEM_ALLOC; cfg_item->scr=$2;}
;

x_elem: X INTEGER
	{CFG_ITEM_ALLOC; cfg_item->x=$2;}
;

y_elem: Y INTEGER
	{CFG_ITEM_ALLOC; cfg_item->y=$2;}
;

%%
/*----------------------------------------------------------------------------*/
/* Instanciate a new item space 
*/
int
cfgparse_alloc() 
{
  TRY_CATCH(cfg_item = malloc(sizeof(cfgfile_item)));
  cfg_item->applet[0]=0;
  cfg_item->id=0;
  cfg_item->scr=0;
  cfg_item->x=0;
  cfg_item->y=0;
  cfg_item->no_update=0;
  return 1;
}

/*----------------------------------------------------------------------------*/
/* Initialise external variables used by the parser
*/
int 
cfgparse_init(vector * vec)
{
  if (vec)
    cfg_vec=vec;
  cfg_item=NULL;
  return vec!=NULL;
}
