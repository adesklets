/*--- tree.c -------------------------------------------------------------------
Copyright (C) 2004, 2005, 2006 Sylvain Fourmanoit <syfou@users.sourceforge.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.   
------------------------------------------------------------------------------*/
/* This implements a minimalistic tree API with error checking.
   
   Have a look at tree.h for a more detailed description. */

/*----------------------------------------------------------------------------*/
#include "tree.h"		/* Tree header */

/*----------------------------------------------------------------------------*/
void *
default_content_free(void* content)
{
  TRY_RESET; TRY(free(content));
  return (TRY_SUCCESS)?NULL:content;
}

/*----------------------------------------------------------------------------*/
tree * 
tree_init(void)
{
  tree * mytree;
  if ((mytree=(tree*)malloc(sizeof(tree)))) {
    if ((mytree->children=vector_init())) {
      mytree->parent=NULL;
      mytree->content=NULL;
      mytree->children->vector_free_item=tree_vector_free_item;
      mytree->tree_free_content=default_content_free;
    } else TRY_CATCH(free(mytree));
  }
  return mytree;
}

/*----------------------------------------------------------------------------*/
int 
tree_push(tree* mytree, void * elem) 
{
  int result = 0;
  tree * item;
  if (mytree && elem)
    if ((item = malloc(sizeof(tree)))) {
      item->content=elem;
      item->parent=mytree;
      item->tree_free_content=mytree->tree_free_content;
      if ((item->children=vector_init()) &&
	  vector_push(mytree->children,item)) {
	item->children->vector_free_item=tree_vector_free_item;
	result=1;
      }
      else free(item);
    }
  return result;
}

/*----------------------------------------------------------------------------*/
int
tree_pop(tree* mytree)
{
  return mytree && vector_pop(mytree->children);
}

/*----------------------------------------------------------------------------*/
int 
tree_inner_iterator(tree * mytree, uint depth, uint pos,
		    tree_callback_func tree_callback, void * params, 
		    tree_iter_type iter_type) 
{
  int i;
  int result=1;
#define TREE_INNER_ITERATOR_CALL \
result&=tree_inner_iterator(mytree->children->content[i],depth+1,i,\
                            tree_callback,params,iter_type)

  if (iter_type==TREE_TOP_DOWN) result&=tree_callback(mytree,depth,pos,params);
  if(iter_type!=TREE_DESTROY)
    for(i=0;i<mytree->children->pos;++i)
      TREE_INNER_ITERATOR_CALL;
  else {
    for(i=mytree->children->pos-1;result&&i>-1;--i,--mytree->children->pos)
      TREE_INNER_ITERATOR_CALL;
    /* Now, this is ugly. Have a look at tree_free_callback for explanation */
    if(!result) {
      mytree->children->content[--mytree->children->pos]=NULL;
      debug("Tree_inner_iterator: memory leak: %d, %d\n",depth,pos);
    }
  }
  
  if (iter_type!=TREE_TOP_DOWN) result&=tree_callback(mytree,depth,pos,params);
  return result;
}

/*----------------------------------------------------------------------------*/
int 
tree_iterate(tree* mytree,tree_callback_func tree_callback,void * params,
	      tree_iter_type iter_type)
{
  return mytree &&
    tree_inner_iterator(mytree,0,0,tree_callback,params, iter_type);
}

/*----------------------------------------------------------------------------*/
/* This function is called only on a terminal leaf, hence its direct use 
   of free() instead of vector_free().

   NOTE: if error reporting is correctly implemented, no attempt is made at 
         correcting them: for now, an error leads to discarding the leaf 
	 (hence a memory leak: I know, its ugly, but at least the global
	 state is preserved!)
*/
int 
tree_free_callback(tree* mytree,uint depth, uint pos, void *params)
{
  int result=0;
  /* Whenever this is executed, we are in a terminal
     leaf: direct freeing is preferable to vector_free. */
  TRY_RESET;
  TRY(free(mytree->children->content));
  if (TRY_SUCCESS) {
    TRY(free(mytree->children));
    if(TRY_SUCCESS) {
      if (mytree->content)
	mytree->content=mytree->tree_free_content(mytree->content);
      TRY(free(mytree));
    }
    result=TRY_SUCCESS;  
  }
#ifdef DEBUG
  if(!result)
    debug("Ouch! Memory leak with tree at 0x%x\n",(uint)mytree);
#endif
  return result;
}

/*----------------------------------------------------------------------------*/
tree * 
tree_free(tree * mytree)
{
  return (tree_iterate(mytree,tree_free_callback,NULL,TREE_DESTROY))?
    NULL:mytree;
}

/*----------------------------------------------------------------------------*/
void *
tree_vector_free_item(void * mytree)
{
  return tree_free((tree*)mytree);
}

/*----------------------------------------------------------------------------*/
