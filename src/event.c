/*--- event.c ----------------------------------------------------------------
Copyright (C) 2004, 2005, 2006 Sylvain Fourmanoit <syfou@users.sourceforge.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.   
------------------------------------------------------------------------------*/
/* Event report functions
 */

/*----------------------------------------------------------------------------*/
#include "event.h"	   	/* Event report header */

/*----------------------------------------------------------------------------*/
pid_t	 events_ppid  = 0;
int      events_echo  = 0;
int      events_delay = 0;
vector * events = NULL;

/*----------------------------------------------------------------------------*/

/* Command output formatting function */
void 
event(char * format, ...)
{
  va_list ap;
#ifdef DEBUG
  va_list apcopy;
#endif
  const char PREFIX[] = "event: ";
  char * event_str;

  if (events_echo && !events_delay) {
    fprintf(stderr,PREFIX);
    va_start(ap,format);
#ifdef DEBUG
    debug("%s",PREFIX);
    va_copy(apcopy,ap);
    vdebug(format,apcopy);
    va_end(apcopy);
#endif
    vfprintf(stderr,format,ap);
    va_end(ap);
    /* Send USR1 signal to ppid process, if defined */
    if (events_ppid)
      kill(events_ppid,SIGUSR1);
  } else {
    if (events || (events=vector_init())) {
      if((event_str=malloc(sizeof(char)*100))) {
	strcpy(event_str,PREFIX);
	va_start(ap,format);
	vsnprintf(event_str+strlen(PREFIX),100,format,ap);
	va_end(ap);
	if (!vector_push(events,event_str)) {
	  debug("Event push error!\n");
	  free(event_str);
	}
      }
#ifdef DEBUG
      else
	debug("Event allocation error!\n");
#endif
    }
#ifdef DEBUG
    else
      debug("Event vector allocation error!\n");
#endif
  }
}

/*----------------------------------------------------------------------------*/
void 
events_purge(void)
{
  int i;
  if (events) {
    for(i=0;i<events->pos;++i) {
      fprintf(stderr,events->content[i]);
      if (events_ppid)
	kill(events_ppid,SIGUSR1);
    }
    events=vector_free(events);
  }
}

/*----------------------------------------------------------------------------*/
