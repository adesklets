/*--- main.c -------------------------------------------------------------------
Copyright (C) 2004, 2005, 2006 Sylvain Fourmanoit <syfou@users.sourceforge.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.   
------------------------------------------------------------------------------*/
#include "adesklets.h"			/* Main application header */

/*----------------------------------------------------------------------------*/
/* Main routine 
   There are only two ways to invoke adesklets: 

   - Without arguments: adesklets is used as a simple
                        program launcher: all desklets
			in ~/.adesklets are run in forked
			process, and adesklets finally exits.
			All adesklets instances for current user 
			that were already running are all killed 
			before the launch.
			Environment variable ADESKLETS_ID 
			is also set according to config_file
			in all children.
   - With arguments:    adesklets is used as an interpreter,
                        either interactive of not.

			If adesklets is used non-iteractively:

   			- If its first free-form argument is an absolute
			file name to something the current user 
			can execute, the windows property 
			will be remembered and updated in
			~/.adesklets and:
				* If ADESKLETS_ID do not exists
				in the environment, it will be set
				to next available unsigned integer
				* If it exists, it will kept and
				used to chose configuration bethween
				different instances of the script.

			- If its first free-form argument is a file name
			to something the current user can execute, 
			either absolute or not, restarting the desklet 
			will be possible.

			- If its first free-form argument is something 
			else, it will simply be discarded.

			If adesklets is used interactively:
			
			- the first free-form argument is always discarded,
			an a prompt is given.

  To complicate things a little further, there is also a customization,
  scriptable shell-based layer on top on this: adesklets_frontend.sh,
  installed by default inside the $pqgdata_dir directory (yet, the binary
  interpreter is fully standalone, and will work without its intervention).
  Basically, this adesklets_frontend.sh script, when detected, is 
  automatically executed before all regular call to the binary. Go read it,
  from ../utils/adesklets_frontend.sh, for details.
*/
int
main(int argc, char ** argv) 
{
#ifdef DEBUG
  /* First of all, initialize debug output if applicable */
  debug_init();
#endif

#ifdef FRONTEND_SHELL_DRIVER
  /* Go through the frontend, as needed */
  if (!getenv("ADESKLETS_FRONTEND")) {
    debug("Frontend signature not detected: try to execve it.\n");
#ifndef DEBUG
    /* Make it possible to run the frontend in-place in case of 
       debugging compilation or help2man extraction */
    if (getenv("ADESKLETS_HELP2MAN"))
#endif
      execv("../utils/adesklets_frontend.sh", argv);
    execv(PKGDATADIR "/adesklets_frontend.sh", argv);
    debug("Could not execute the frontend.\n");
  }
#endif

  /* Now, perform the core execution */
  if(argc>1) {
    /* adesklets used as an interpreter */
    if (adesklets_init(argc,argv)) {
      adesklets_events_loop();
      adesklets_free();
    }
  } else {
    /* adesklets used as a desklets launcher */
#ifdef FRONTEND_SHELL_DRIVER
    unsetenv("ADESKLETS_FRONTEND");
#endif
    cfgfile_loadall();
  }
}
